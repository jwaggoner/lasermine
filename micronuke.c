#include "g_local.h"
#include "lasrmine.h"
#include "micronuke.h"
#include "admin.h"

void Nuke_Quake (edict_t *ent)
{
	edict_t *myquake;
	
	myquake = G_Spawn();
	myquake->targetname = "t999";
	myquake->classname = "target_earthquake";
	myquake->speed = 500;
	myquake->count = 5;
	ED_CallSpawn (myquake);
	myquake->timestamp = level.time + myquake->count;
	myquake->nextthink = level.time + FRAMETIME;
	myquake->activator = ent;
	myquake->last_move_time = 0;
}

qboolean G_Within_Radius(vec3_t start, vec3_t end, float rad)
{ 
	vec3_t eorg={0,0,0};
	int j;
	for (j=0; j<3; j++)
		eorg[j]=abs(start[j]-end[j]);
	return (VectorLength(eorg) < rad); 
} 

void SetMNRange(edict_t *ent)
{
	float dist;  // distance to closest micronuke
	float mndist ; // distance to a nuke (one currently being tested in loop)
	qboolean nukefound; 
	edict_t *nuke = NULL;
	vec3_t	v;  //vector to micronuke
	vec3_t	dest;
	trace_t	trace;
	qboolean ouch;

	dist = 9999;
	nukefound = false;
	
	//loop through all nukes and find closest one
	while ((nuke = G_Find (nuke, FOFS(classname), "micronuke")) != NULL)
	{
		nukefound = true;
		
		VectorSubtract (ent->s.origin, nuke->s.origin, v);
		mndist = VectorLength(v);
	
		if (mndist < dist)
			dist = mndist; //set dist to this nuke (closest)

		// find out if the nuke will kill client
		trace = gi.trace (nuke->s.origin, vec3_origin, vec3_origin, ent->s.origin, nuke, MASK_SOLID);
		if (trace.fraction == 1.0)
			ouch = true;
		else
		{
			VectorCopy (ent->s.origin, dest);
			dest[0] += 15.0;
			dest[1] += 15.0;
			trace = gi.trace (nuke->s.origin, vec3_origin, vec3_origin, dest, nuke, MASK_SOLID);
			if (trace.fraction == 1.0)
				ouch = true;
			else
			{
				VectorCopy (ent->s.origin, dest);
				dest[0] += 15.0;
				dest[1] -= 15.0;
				trace = gi.trace (nuke->s.origin, vec3_origin, vec3_origin, dest, nuke, MASK_SOLID);
				if (trace.fraction == 1.0)
					ouch = true;
				else
				{
					VectorCopy (ent->s.origin, dest);
					dest[0] -= 15.0;
					dest[1] += 15.0;
					trace = gi.trace (nuke->s.origin, vec3_origin, vec3_origin, dest, nuke, MASK_SOLID);
					if (trace.fraction == 1.0)
						ouch = true;
					else
					{
						VectorCopy (ent->s.origin, dest);
						dest[0] -= 15.0;
						dest[1] -= 15.0;
						trace = gi.trace (nuke->s.origin, vec3_origin, vec3_origin, dest, nuke, MASK_SOLID);
						if (trace.fraction == 1.0)
							ouch = true;
						else
							ouch = false;
					}
				}
			}
		}
	}

	if (dist > 9999)
		dist = 9999;

	if (nukefound)
		ent->client->ps.stats[STAT_MNRANGE] = dist;
	else
		ent->client->ps.stats[STAT_MNRANGE] = 0;

	if (dist < nuke_radius->value)	//client in danger, within radiation rangr
		ent->client->ps.stats[STAT_RADIATION_PIC] = gi.imageindex ("a_blaster"); 
	else if (ouch == true)	//client in danger, within line of sight range and in line of sight
		ent->client->ps.stats[STAT_RADIATION_PIC] = gi.imageindex ("a_blaster");
	else	//client not in danger (except for shockwave)
		ent->client->ps.stats[STAT_RADIATION_PIC] = 0;
	
}

void nuke_lasermines (edict_t *self)
{
	edict_t	*ent;

	ent = NULL;

//	gi.dprintf("\n\n***killing lasermines***\n\n");
	while ((ent = findradius(ent, self->s.origin, nuke_radius->value)) != NULL)
	{
//		gi.dprintf("%s \n",ent->classname);

		if (ent -> takedamage)  //( people or bodies for fun )
		{
			T_Damage (ent, world, world, vec3_origin, ent->s.origin, vec3_origin, 1000, 0, 0, MOD_MICRONUKE);
			gi.WriteByte (svc_temp_entity);
			gi.WriteByte (TE_BFG_LASER);
			gi.WritePosition (self->s.origin);
			gi.WritePosition (ent->s.origin);
			gi.multicast (self->s.origin, MULTICAST_PHS);

			// make 'em scream
			if (rand()&1)
				gi.sound (ent, CHAN_VOICE, gi.soundindex("player/burn1.wav"), 1, ATTN_NORM, 0);
			else
				gi.sound (ent, CHAN_VOICE, gi.soundindex("player/burn2.wav"), 1, ATTN_NORM, 0);
			continue;
		}


		if (Q_stricmp(ent->classname,"lasermine")!=0)
			continue;

		//COOK 'DEM LASERMINES
		gi.WriteByte (svc_temp_entity);
		gi.WriteByte (TE_BFG_LASER);
		gi.WritePosition (self->s.origin);
		gi.WritePosition (ent->s.origin);
		gi.multicast (self->s.origin, MULTICAST_PHS);

		if (Q_stricmp(ent->owner->classname,"hgrenade")==0)  // don't explode grenade if it's already gone!
			Grenade_Explode (ent->owner);
		G_FreeEdict (ent);  
	}
}

void Nuke_Shockwave (edict_t *ent)
{
	edict_t *player=NULL;
	vec3_t forward,backward,right,left,up,v;
	int i;
	float dist;
		
	// For some reason, must use VectorSet().. 
	VectorSet(forward,0,1,0);
	VectorSet(right,1,0,0);
	VectorSet(backward,0,-1,0);
	VectorSet(left,-1,0,0);

	// Throw any players in 4000 units radius.
	for(i=0;i < game.maxclients;i++)
	{
		player=g_edicts+i+1; 
		if (!G_ClientInGame(player))
			continue;
		if (ent==player)
			continue;
		if (!G_Within_Radius(player->s.origin, ent->s.origin, 4000))
			continue;
		VectorSubtract (player->s.origin, ent->s.origin, v);
		dist = VectorLength(v);
		AngleVectors(player->s.angles, forward, right, up);
		VectorMA(player->velocity, 1200 * (1000 / dist), backward, player->velocity);
		VectorMA(player->velocity, 1200 * (1000 / dist), up, player->velocity);
	} // end for 
} 



void nuke_explode2 (edict_t *ent)
{
	//create second explosion sounds
	gi.positioned_sound (ent->s.origin, ent, CHAN_AUTO, gi.soundindex ("world/explod1.wav"), 1.0, ATTN_NONE, 0);
	
	//destroy any laser mines
	Nuke_Quake(ent);
	nuke_lasermines (ent);
	Nuke_Shockwave (ent);
	
	//do some damage
	T_RadiusDamage(ent, ent->owner, ent->dmg, ent->enemy, ent->dmg_radius, MOD_MICRONUKE);

	//create small explosion at nuke
	gi.WriteByte (svc_temp_entity);
	gi.WriteByte (TE_ROCKET_EXPLOSION);
	gi.WritePosition (ent->s.origin);
	gi.multicast (ent->s.origin, MULTICAST_PHS);

	//release nuke from game	
	G_FreeEdict (ent);
}


void nuke_explode1 (edict_t *ent)
{
	//first part of explosion
	//just sound
	gi.positioned_sound (ent->s.origin, ent, CHAN_AUTO, gi.soundindex ("misc/tele_up.wav"), 1.0, ATTN_NONE, 0);
	
	ent->nextthink = level.time + 1.2;
	ent->think = nuke_explode2;
}

void nuke_countdown(edict_t *ent)
{
// does earthquake
// and countdown 

		edict_t *myquake;
		//edict_t *countdown;

		myquake = G_Spawn();
//		countdown = G_Spawn();
	

		myquake->targetname = "t999";
		myquake->classname = "target_earthquake";
		myquake->count = 8;
		ED_CallSpawn (myquake);
	
		myquake->timestamp = level.time + myquake->count;
		myquake->nextthink = level.time + FRAMETIME;
		myquake->activator = ent;
	    myquake->last_move_time = 0;

  
		//VectorClear(countdown->s.origin);	
//		countdown->noise_index = gi.soundindex ("world/10_0.wav");
	//	gi.positioned_sound (nuke->s.origin, nuke, CHAN_AUTO, gi.soundindex ("world/10_0.wav"), .75, ATTN_NONE, 0);
}


void drop_nuke (edict_t *ent)
{
	edict_t *nuke;
	char message[256];
	int cells;
	int slugs;

	if (!allownuke->value)
	{
		gi.cprintf(ent, PRINT_HIGH, "Micronukes are disabled on this server.\n");
		return;
	}

	cells = ent->client->pers.inventory[ITEM_INDEX(FindItem("cells"))] ;
	slugs = ent->client->pers.inventory[ITEM_INDEX(FindItem("slugs"))] ;

//test spectator
	if ( ent->client->resp.ctf_team == CTF_NOTEAM)
		return;
		
//test ammo
	if ( (cells < nuke_cells->value ) || (slugs < nuke_slugs->value) )
	{
		cells = nuke_cells->value - cells;
		slugs = nuke_slugs->value - slugs;
		
		if (cells < 0)
			cells = 0;
		if (slugs < 0)
			slugs = 0;
		
		
		strcpy (message, "You need ");

		if (cells != 0) //need cells
			sprintf(message,"%s%d more cells ",message,cells);
		
		if ((cells * slugs)!=0)  // need both
			strcat (message, "and ");
		
		if (slugs!= 0) //need slugs
			sprintf(message,"%s%d more slugs ",message,slugs);

		gi.centerprintf(ent, "%s\n", message);
		return;
	}
	
	ent->client->pers.inventory[ITEM_INDEX(FindItem("cells"))] -= nuke_cells->value;
	ent->client->pers.inventory[ITEM_INDEX(FindItem("slugs"))] -= nuke_slugs->value;


//create the nuke	
	nuke = G_Spawn();
	nuke ->	s.effects	   |= EF_COLOR_SHELL;
	nuke -> s.renderfx		= RF_GLOW | RF_SHELL_RED;
	nuke -> s.modelindex	= gi.modelindex ("models/items/ammo/nuke/tris.md2");
	nuke -> s.sound			= gi.soundindex ("world/klaxon1.wav");
	nuke -> classname		= "micronuke";
	nuke -> owner			= nuke;
	nuke -> think			= nuke_explode1;
	nuke -> dmg				= 2000;
	nuke -> dmg_radius		= nuke_radius2->value;

//place nuke (change to drop)	
	VectorCopy (ent->s.origin, nuke->s.origin);
	nuke->s.origin[2] -= 10;   // set on ground

//add to world
	gi.linkentity (nuke);
	gi.bprintf(PRINT_HIGH, "*** Warning! Micronuke activated by %s! ***\n",ent->client->pers.netname);
	
	//nuke_countdown(ent);

	nuke -> nextthink = level.time + 10;

	gi.positioned_sound (nuke->s.origin, nuke, CHAN_AUTO, gi.soundindex ("world/10_0.wav"), .75, ATTN_NONE, 0);
	gi.positioned_sound (nuke->s.origin, nuke, CHAN_AUTO, gi.soundindex ("world/klaxon2.wav"), 1, ATTN_NORM, 0);

//	gi.centerprintf(ent, "Micronuke activated.  Evacuate Area...\n");
}
