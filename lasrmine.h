// FUNCTIONS //////////////////////////////////////////////

void	target_laser_on (edict_t *self);
void	target_laser_off (edict_t *self);
void	Place_LaserMine (edict_t *ent, cplane_t *plane, csurface_t *surf);
void	pre_target_laser_think (edict_t *self);
void	target_lasermine_think (edict_t *self);
void	spawn_outlandgrenade(void);

// built in quake2 routines 
void target_laser_use (edict_t *self, edict_t *other, edict_t *activator);
void target_laser_think (edict_t *self);
void target_laser_on (edict_t *self);
void target_laser_off (edict_t *self);
void ED_CallSpawn(edict_t *ent);
void Grenade_Explode(edict_t *ent);
