#include "g_local.h"
#include "oldmaps.h"

char *ReadTextFile(char *filename)
{
    FILE        *fp;
    char        *filestring = NULL;
    long int    i;
    int         ch;

    while (true) {
        fp = fopen(filename, "r");
        if (!fp) break;

        for (i=0; (ch = fgetc(fp)) != EOF; i++);

        filestring = gi.TagMalloc(i+1, TAG_LEVEL);
        if (!filestring) break;

        fseek(fp, 0, 0);
        for (i=0; (ch = fgetc(fp)) != EOF; i++)
            filestring[i] = ch;
        filestring[i] = '\0';

        break;
    }

    if (fp) fclose(fp);

    return(filestring);
}

char *LoadEntities(char *mapname, char *entities)
{
    char   entfilename[MAX_QPATH] = "";
    char   *newentities;
    int    i, islefn;
    cvar_t *basedir;
    cvar_t *gamedir;

    basedir = gi.cvar("basedir", "", 0);
    gamedir = gi.cvar("gamedir", "", 0);

#ifdef _WIN32
    sprintf(entfilename, "%s\\%s\\maps\\ents\\", basedir->string, gamedir->string);
#else
    sprintf(entfilename, "%s/%s/maps/ents/", basedir->string, gamedir->string);
#endif
    islefn = strlen(entfilename);
    for (i=0; mapname[i]; i++)
        entfilename[i + islefn] = tolower(mapname[i]);
    entfilename[i + islefn] = '\0';
    strcat(entfilename, ".ent");

    newentities = ReadTextFile(entfilename);

    if (newentities)
		return(newentities);
	else
        return(entities);
}


void GetNextMapFromFile(edict_t *ent)
{
	char		filename[MAX_QPATH] = "";
    FILE        *fp;
    char        *filestring = NULL;
    cvar_t		*basedir;
    cvar_t		*gamedir;
	char		message[200];
	char		mapname[200];
	

    basedir = gi.cvar("basedir", "", 0);
    gamedir = gi.cvar("gamedir", "", 0);

    sprintf(filename, "%s\\%s\\maps.lst", basedir->string, gamedir->string);

	//gi.dprintf("filename:  %s\n" ,filename);
    
    //gi.dprintf("current map:  %s \n", level.mapname);

	fp = fopen(filename, "r");
	while (fp) 
	{
 		if (fscanf( fp, "%s", mapname ) == EOF ) break;
		fgets( message, 1000 ,fp);
		if (Q_stricmp (level.mapname, mapname)) break;
//		gi.dprintf("mapname:  %s - %s\n" ,mapname,message);

	}
	//found current map, now find next one.
	if (fscanf( fp, "%s", mapname ) != EOF )
	{
		fgets( message, 1000 ,fp);

	}
	else
	{
        fseek(fp, 0, 0);
		if (fscanf( fp, "%s", mapname ) != EOF )
		{
			fgets( message, 1000 ,fp);

		}	
	}

	gi.dprintf("cur: %s, next: %s - %s\n",level.mapname, mapname,message);
    
	if (fp) fclose(fp);
 }

