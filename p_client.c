#include "g_local.h"
#include "m_player.h"
#include "admin.h"
#include "g_lights.h"
/* SL - Include Standard Logging Header File */
#include "stdlog.h"
/* GS - Include Standard Logging Q2 Helper Functions Header File */
#include "gslog.h"
/* SL - Default Player Name incase of name clash */
#define NAME_CLASH_STR    "Same Name Clash"

void CTFTeam_f (edict_t *ent, int team);

//qboolean ZbotCheck(edict_t *ent, usercmd_t *ucmd);

void ClientUserinfoChanged (edict_t *ent, char *userinfo);

void SP_misc_teleporter_dest (edict_t *ent);

//
// Gross, ugly, disgustuing hack section
//

// this function is an ugly as hell hack to fix some map flaws
//
// the coop spawn spots on some maps are SNAFU.  There are coop spots
// with the wrong targetname as well as spots with no name at all
//
// we use carnal knowledge of the maps to fix the coop spot targetnames to match
// that of the nearest named single player spot

static void SP_FixCoopSpots (edict_t *self)
{
	edict_t	*spot;
	vec3_t	d;

	spot = NULL;

	while(1)
	{
		spot = G_Find(spot, FOFS(classname), "info_player_start");
		if (!spot)
			return;
		if (!spot->targetname)
			continue;
		VectorSubtract(self->s.origin, spot->s.origin, d);
		if (VectorLength(d) < 384)
		{
			if ((!self->targetname) || stricmp(self->targetname, spot->targetname) != 0)
			{
//				gi.dprintf("FixCoopSpots changed %s at %s targetname from %s to %s\n", self->classname, vtos(self->s.origin), self->targetname, spot->targetname);
				self->targetname = spot->targetname;
			}
			return;
		}
	}
}

// now if that one wasn't ugly enough for you then try this one on for size
// some maps don't have any coop spots at all, so we need to create them
// where they should have been

static void SP_CreateCoopSpots (edict_t *self)
{
	edict_t	*spot;

	if(stricmp(level.mapname, "security") == 0)
	{
		spot = G_Spawn();
		spot->classname = "info_player_coop";
		spot->s.origin[0] = 188 - 64;
		spot->s.origin[1] = -164;
		spot->s.origin[2] = 80;
		spot->targetname = "jail3";
		spot->s.angles[1] = 90;

		spot = G_Spawn();
		spot->classname = "info_player_coop";
		spot->s.origin[0] = 188 + 64;
		spot->s.origin[1] = -164;
		spot->s.origin[2] = 80;
		spot->targetname = "jail3";
		spot->s.angles[1] = 90;

		spot = G_Spawn();
		spot->classname = "info_player_coop";
		spot->s.origin[0] = 188 + 128;
		spot->s.origin[1] = -164;
		spot->s.origin[2] = 80;
		spot->targetname = "jail3";
		spot->s.angles[1] = 90;

		return;
	}
}


/*QUAKED info_player_start (1 0 0) (-16 -16 -24) (16 16 32)
The normal starting point for a level.
*/
void SP_info_player_start(edict_t *self)
{
	if (!coop->value)
		return;
	if(stricmp(level.mapname, "security") == 0)
	{
		// invoke one of our gross, ugly, disgusting hacks
		self->think = SP_CreateCoopSpots;
		self->nextthink = level.time + FRAMETIME;
	}
}

/*QUAKED info_player_deathmatch (1 0 1) (-16 -16 -24) (16 16 32)
potential spawning position for deathmatch games
*/
void SP_info_player_deathmatch(edict_t *self)
{
	if (!deathmatch->value)
	{
		G_FreeEdict (self);
		return;
	}
	SP_misc_teleporter_dest (self);
}

/*QUAKED info_player_coop (1 0 1) (-16 -16 -24) (16 16 32)
potential spawning position for coop games
*/

void SP_info_player_coop(edict_t *self)
{
	if (!coop->value)
	{
		G_FreeEdict (self);
		return;
	}

	if((stricmp(level.mapname, "jail2") == 0)   ||
	   (stricmp(level.mapname, "jail4") == 0)   ||
	   (stricmp(level.mapname, "mine1") == 0)   ||
	   (stricmp(level.mapname, "mine2") == 0)   ||
	   (stricmp(level.mapname, "mine3") == 0)   ||
	   (stricmp(level.mapname, "mine4") == 0)   ||
	   (stricmp(level.mapname, "lab") == 0)     ||
	   (stricmp(level.mapname, "boss1") == 0)   ||
	   (stricmp(level.mapname, "fact3") == 0)   ||
	   (stricmp(level.mapname, "biggun") == 0)  ||
	   (stricmp(level.mapname, "space") == 0)   ||
	   (stricmp(level.mapname, "command") == 0) ||
	   (stricmp(level.mapname, "power2") == 0) ||
	   (stricmp(level.mapname, "strike") == 0))
	{
		// invoke one of our gross, ugly, disgusting hacks
		self->think = SP_FixCoopSpots;
		self->nextthink = level.time + FRAMETIME;
	}
}


/*QUAKED info_player_intermission (1 0 1) (-16 -16 -24) (16 16 32)
The deathmatch intermission point will be at one of these
Use 'angles' instead of 'angle', so you can set pitch or roll as well as yaw.  'pitch yaw roll'
*/
void SP_info_player_intermission(void)
{
}


//=======================================================================


void player_pain (edict_t *self, edict_t *other, float kick, int damage)
{
	// player pain is handled at the end of the frame in P_DamageFeedback
}


qboolean IsFemale (edict_t *ent)
{
	char		*info;

	// Make sure ent exists!
	if (!G_EntExists(ent))
		return false;

	info = Info_ValueForKey (ent->client->pers.userinfo, "skin");
	if (info[0] == 'f' || info[0] == 'F')
		return true;
	return false;
}

qboolean IsNeutral (edict_t *ent)
{
  char    *info;
// Make sure ent exists!
  if (!G_EntExists(ent)) return false;


  if (!ent->client)
    return false;

  info = Info_ValueForKey (ent->client->pers.userinfo, "gender");
  if (info[0] != 'f' && info[0] != 'F' && info[0] != 'm' && info[0] != 'M')
    return true;
  return false;
}

void ClientObituary (edict_t *self, edict_t *inflictor, edict_t *attacker)
{
	int mod, i;
	char *message[16];
	char *message2[16];
	qboolean ff;

	ff = meansOfDeath & MOD_FRIENDLY_FIRE;
  	mod = meansOfDeath & ~MOD_FRIENDLY_FIRE;

  // If no victim then no obit!
  if (!G_EntExists(self)) return;


		for (i = 0; i < 16; i++)
		{
			message[i] = NULL;
			message2[i] = "";
		} //end for
		
		switch (mod)
		{
		case MOD_SUICIDE:
			message[0] = "commits suicide";
			message[1] = "takes the easy way out";
			if (IsNeutral(self))
			{
				message[2] = "has fragged itself";
				message[3] = "took its own life";
			}
			else if (IsFemale(self))
			{
				message[2] = "has fragged herself";
				message[3] = "took her own life";
			} //end if
			else
			{
				message[2] = "has fragged himself";
				message[3] = "took his own life";
			} //end else
			message[4] = "can be scraped off the pavement";
			break;
		case MOD_FALLING:
			message[0] = "cratered";
			message[1] = "discovered the effects of gravity";
			break;
		case MOD_CRUSH:
			message[0] = "was squished";
			message[1] = "was squeezed like a ripe grape";
			message[2] = "turned to juice";
			break;
		case MOD_WATER:
			message[0] = "sank like a rock";
			message[1] = "tried unsuccesfully to breathe water";
			message[2] = "tried to immitate a fish";
			message[3] = "must learn when to breathe";
			message[5] = "needs to learn how to swim";
			message[6] = "took a long walk off a short pier";
			message[7] = "might want to use a rebreather next time";
			if (IsNeutral(self))
			{
				message[4] = "thought it didn't need a rebreather";
			} //end if
			else if (IsFemale(self))
			{
				message[4] = "thought she didn't need a rebreather";
			} //end if
			else
			{
				message[4] = "thought he didn't need a rebreather";
			} //end else
			break;
		case MOD_SLIME:
			message[0] = "melted";
			message[1] = "was dissolved";
			message[2] = "sucked slime";
			message[3] = "found an alternative way to die";
			message[4] = "needs more slime-resistance";
			message[5] = "might try on an environmental suit next time";
			break;
		case MOD_LAVA:
			message[0] = "does a back flip into the lava";
			message[1] = "was fried to a crisp";
			message[2] = "thought that lava was water";
			message[3] = "turned into a real hothead";
			message[4] = "thought lava was 'funny water'";
			message[5] = "tried to hide in the lava";
			if (IsNeutral(self))
			{
				message[6] = "thought it was fire resistant";
				message[7] = "tried to emulate the demigod";
				message[8] = "needs to rebind its 'strafe' keys";
			} //end if
			else if (IsFemale(self))
			{
				message[6] = "thought she was fire resistant";
				message[7] = "tried to emulate the goddess Pele";
				message[8] = "needs to rebind her 'strafe' keys";
			} //end if
			else
			{
				message[6] = "thought he was fire resistant";
				message[7] = "tried to emulate the god of hell-fire";
				message[8] = "needs to rebind his 'strafe' keys";
			} //end else
			break;
		case MOD_EXPLOSIVE:
		case MOD_BARREL:
			message[0] = "blew up";
			break;
		case MOD_EXIT:
			message[0] = "found a way out";
			message[1] = "had enough for today";
			message[2] = "exited, stage left";
			message[3] = "has returned to real life(tm)";
			break;
		case MOD_TARGET_LASER:
			message[0] = "found a laser";
			break;
		case MOD_TARGET_BLASTER:
			message[0] = "got blasted";
			break;
		case MOD_BOMB:
		case MOD_SPLASH:
		case MOD_TRIGGER_HURT:
			message[0] = "was in the wrong place";
			message[1] = "shouldn't play with equipment";
			message[2] = "can't move around moving objects";
			break;
		case MOD_MICRONUKE:
			message[0] = "found a micronuke";
			message[1] = "succumbed to radiation poisoning";
			message[2] = "couldn't move fast enough";
			if (IsNeutral(self))
				message[3] = "lost its head...and arms...and legs...";
			else if (IsFemale(self))
				message[3] = "lost her head...and arms...and legs...";
			else
				message[3] = "lost his head...and arms...and legs...";
			break;
		}
    	if (attacker == self)
		{
			switch (mod)
			{
			case MOD_HELD_GRENADE:
				message[0] = "tried to put the pin back in";
				message[2] = "got the red and blue wires mixed up";
				if (IsNeutral(self))
				{
					message[1] = "held its grenade too long";
					message[3] = "tried to disassemble it's own grenade";
				} //end if
				else if (IsFemale(self))
				{
					message[1] = "held her grenade too long";
					message[3] = "tried to disassemble her own grenade";
				} //end if
				else
				{
					message[1] = "held his grenade too long";
					message[3] = "tried to disassemble his own grenade";
				} //end else
				break;
			case MOD_HG_SPLASH:
//T-MEK
				if (IsFemale(self))
				{
					message[0] = "tripped on her own lasermine";
					message[1] = "didn't know that tripwire was hers";
					message[2] = "needs to watch her step";
				}
				else if (IsNeutral(self))
				{
					message[0] = "tripped on its own lasermine";
					message[1] = "didn't know that tripwire was its own";
					message[2] = "needs to watch its step";
				}
				else
				{
					message[0] = "tripped on his own lasermine";
					message[1] = "didn't know that tripwire was his";
					message[2] = "needs to watch his step";
				}
				break;
//T-MEK

			case MOD_G_SPLASH:
				message[2] = "tried to grenade-jump unsuccessfully";
				message[3] = "tried to play football with a grenade";
				message[4] = "shouldn't mess around with explosives";
				if (IsNeutral(self))
				{
					message[0] = "tripped on it's own grenade";
					message[1] = "stepped on it's own pineapple";
				}
				else if (IsFemale(self))
				{
					message[0] = "tripped on her own grenade";
					message[1] = "stepped on her own pineapple";
				} //end if
				else
				{
					message[0] = "tripped on his own grenade";
					message[1] = "stepped on his own pineapple";
				} //end else
				break;
			case MOD_R_SPLASH:
				message[2] = "knows didley squatt about rocket launchers";
				message[4] = "thought up a novel new way to fly";
				if (IsNeutral(self))
				{
					message[0] = "blew itself up";
					message[1] = "thought it was Werner von Braun";
					message[3] = "thought it had more health";
					message[5] = "found it's own rocketlauncher's trigger";
					message[6] = "thought it had more armor on";
					message[7] = "blew itself to kingdom come";
				} //end if
				else if (IsFemale(self))
				{
					message[0] = "blew herself up";
					message[1] = "thought she was Werner von Braun";
					message[3] = "thought she had more health";
					message[5] = "found her own rocketlauncher's trigger";
					message[6] = "thought she had more armor on";
					message[7] = "blew herself to kingdom come";
				} //end if	
				else
				{
					message[0] = "blew himself up";
					message[1] = "thought he was Werner von Braun";
					message[3] = "thought he had more health";
					message[5] = "found his own rocketlauncher's trigger";
					message[6] = "thought he had more armor on";
					message[7] = "blew himself to kingdom come";
				} //end else
				break;
			case MOD_BFG_BLAST:
				message[0] = "should have used a smaller gun";
				message[1] = "shouldn't play with big guns";
				message[2] = "doesn't know how to work the BFG";
				message[3] = "has trouble using big guns";
				message[4] = "can't distinguish which end is which with the BFG";
				message[5] = "should try to avoid using the BFG near obstacles";
				message[6] = "tried to BFG-jump unsuccesfully";
				break;

			default:
				message[1] = "commited suicide";
				message[2] = "went the way of the dodo";
				message[3] = "thought 'kill' was a funny console command";
				message[4] = "wanted one frag less";
				if (IsNeutral(self))
				{
					message[0] = "killed itself";
					message[5] = "thought it had one many frags";
				} //end if
				else if (IsFemale(self))
				{
					message[0] = "killed herself";
					message[5] = "thought she had one many frags";
				} //end if
				else
				{
					message[0] = "killed himself";
					message[5] = "thought he had one many frags";
				} //end else
				break;
			} //end switch
		} //end if
		if (message[0])
		{
			for (i = 0; i < 16; i++)
			{
				if (!message[i]) break;
			} //end for
			i = random() * (float) i;
			gi.bprintf (PRINT_MEDIUM, "%s %s.\n", self->client->pers.netname, message[i]);
			self->client->resp.score--;	
			self->enemy = NULL;
			return;
		} //end if

    	self->enemy = attacker;
		if (attacker && attacker->client)
		{
			switch (mod)
			{
				case MOD_BLASTER:
					message[0] = "(quakeweenie) was massacred by";
					message2[0] = " (quakegod)!!!";
					message[1] = "was killed with the wimpy blaster by";
					message[2] = "died a wimp's death by";
					message[3] = "can't even avoid a blaster from";
					message[4] = "was blasted by";
					break;
				case MOD_SHOTGUN:
					message[1] = "was gunned down by";
					if (IsNeutral(self))
					{
						message[0] = "found itself on the wrong end of";
						message2[0] = "'s gun";
					} //end if
					else if (IsFemale(self))
					{
						message[0] = "found herself on the wrong end of";
						message2[0] = "'s gun";
					} //end if
					else
					{
						message[0] = "found himself on the wrong end of";
						message2[0] = "'s gun";
					} //end else
					break;
				case MOD_SSHOTGUN:
					message[0] = "was blown away by";
					message2[0] = "'s super shotgun";
					if (IsNeutral(self))
					{
						message[0] = "had its ears cleaned out by";
						message2[0] = "'s super shotgun";
					} //end if
					else if (IsFemale(self))
					{
						message[0] = "had her ears cleaned out by";
						message2[0] = "'s super shotgun";
					} //end if
					else
					{
						message[0] = "had his ears cleaned out by";
						message2[0] = "'s super shotgun";
					} //end else
					message[3] = "was put full of buckshot by";
					break;
				case MOD_MACHINEGUN:
					message[0] = "was machinegunned by";
					message[1] = "was filled with lead by";
					message[2] = "was put full of lead by";
					message[3] = "was pumped full of lead by";
					message[4] = "ate lead dished out by";
					message[5] = "eats lead from";
					message[6] = "bites the bullet from";
					break;
				case MOD_CHAINGUN:
					message[0] = "was cut in half by";
					message2[0] = "'s chaingun";
					message[2] = "was turned into a strainer by";
					message[3] = "was put full of holes by";
					message[4] = "couldn't avoid death by painless from";
					if (IsNeutral(self))
					{
						message[1] = "was pumped so full of lead by";
						message2[1] = " you can call it a pencil";
					} //end if
					else if (IsFemale(self))
					{
						message[1] = "was pumped so full of lead by";
						message2[1] = " you can call her a pencil";
					} //end if
					else
					{
						message[1] = "was pumped so full of lead by";
						message2[1] = " you can call him a pencil";
					} //end else
					break;
				case MOD_GRENADE:
					message[0] = "was popped by";
					message2[0] = "'s grenade";
					message[1] = "caught";
					message2[1] = "'s grenade in the head";
					message[2] = "tried to headbutt the grenade of";
					break;
				case MOD_G_SPLASH:
					message[0] = "was shredded by";
					message2[0] = "'s shrapnel";
					break;
				case MOD_ROCKET:
					message[0] = "ate";
					message2[0] = "'s rocket";
					message[1] = "sucked on";
					message2[1] = "'s boomstick";
					message[2] = "tried to play 'dodge the missile' with";
					message[3] = "tried the 'patriot move' on the rocket from";
					message[4] = "had a rocket stuffed down the throat by";
					message[5] = "got a rocket up the tailpipe by";
					message[6] = "tried to headbutt";
					message2[6] = "'s rocket";
					break;
				case MOD_R_SPLASH:
					message[0] = "almost dodged";
					message2[0] = "'s rocket";
					message[1] = "was spread around the place by";
					message[2] = "was gibbed by";
					message[3] = "has been blown to smithereens by";
					message[4] = "was blown to itsie bitsie tiny pieces by";
					break;
				case MOD_HYPERBLASTER:
					message[0] = "was melted by";
					message2[0] = "'s hyperblaster";
					message[1] = "was used by";
					message2[1] = " for target practice";
					message[2] = "was hyperblasted by";
					message[3] = "was pumped full of cells by";
					message[4] = "couldn't outrun the hyperblaster from";
					break;
				case MOD_RAILGUN:
					message[0] = "was railed by";
					message[2] = "played 'catch the slug' with";
					message[4] = "bites the slug from";
					message[5] = "caught the slug from";
					if (IsNeutral(self))
					{
						message[1] = "got a slug put through it by";
						message[3] = "was corkscrewed through it's head by";
						message[6] = "had its body pierced with a slug from";
						message[7] = "had its brains blown out by";
					} //end if
					else if (IsFemale(self))
					{
						message[1] = "got a slug put through her by";
						message[3] = "was corkscrewed through her head by";
						message[6] = "had her body pierced with a slug from";
						message[7] = "had her brains blown out by";
					} //end if
					else
					{
						message[1] = "got a slug put through him by";
						message[3] = "was corkscrewed through his head by";
						message[6] = "had his body pierced with a slug from";
						message[7] = "had his brains blown out by";
					} //end else
					break;
				case MOD_BFG_LASER:
					message[0] = "saw the pretty lights from";
					message2[0] = "'s BFG";
					message[1] = "was diced by the BFG from";
					break;
				case MOD_BFG_BLAST:
					message[0] = "was disintegrated by";
					message2[0] = "'s BFG blast";
					message[1] = "was flatched with the green light by";
					message2[1] = "";
					break;
				case MOD_BFG_EFFECT:
					message[0] = "couldn't hide from";
					message2[0] = "'s BFG";
					message[1] = "tried to soak up green energy from";
					message2[1] = "'s BFG";
					message[2] = "was energized with 50 cells by";
					message[3] = "doesn't know when to run from";
					message[4] = "'saw the light' from";
					break;
				case MOD_HANDGRENADE:
					message[0] = "inspected";
					message2[0] = "'s lasermine";
					break;
				case MOD_HG_SPLASH:
					message[0] = "broke";
					message2[0] = "'s beam";
					break;
				case MOD_HELD_GRENADE:
					message[0] = "feels";
					message2[0] = "'s pain";
					break;
				case MOD_TELEFRAG:
					message[0] = "tried to invade";
					message2[0] = "'s personal space";
					message[1] = "is less telefrag aware than";
					message[2] = "should appreciate scotty more like";
					break;
			//	case MOD_GRAPPLE:
			//		message[0]= "was fried alive by";
			//		message2[0] = "'s laser hook";//raven laser hook addition
			//		break;
//ZOID
			case MOD_GRAPPLE:
				message[0] = "was caught by";
				message2[0] = "'s grapple";
				break;
//ZOID
      }
     if (message[0])
			{
				for (i = 0; i < 16; i++)
				{
					if (!message[i]) break;
				} //end for
				i = (random()-0.01) * (float) i;
				gi.bprintf (PRINT_MEDIUM,"%s %s %s%s\n", self->client->pers.netname, message[i], attacker->client->pers.netname, message2[i]);

				if (deathmatch->value)
				{
					if (ff)
						attacker->client->resp.score--;
					else
						attacker->client->resp.score++;
				}
				return;
			}
		}
	
	gi.bprintf (PRINT_MEDIUM,"%s died.\n", self->client->pers.netname);
	if (deathmatch->value)
	self->client->resp.score--;
}

void Touch_Item(edict_t *ent, edict_t *other, cplane_t *plane, csurface_t *surf);

void TossClientWeapon (edict_t *self)
{
	gitem_t		*item;
	edict_t		*drop;
	qboolean	quad;
	float		spread;

	// Make sure ent exists!
	if (!G_EntExists(self))
		return;

	
	if (!deathmatch->value)
		return;

	item = self->client->pers.weapon;
	if (! self->client->pers.inventory[self->client->ammo_index] )
		item = NULL;
	if (item && (strcmp (item->pickup_name, "Blaster") == 0))
		item = NULL;

	if (!((int)(dmflags->value) & DF_QUAD_DROP))
		quad = false;
	else
		quad = (self->client->quad_framenum > (level.framenum + 10));

	if (item && quad)
		spread = 22.5;
	else
		spread = 0.0;

	if (item)
	{
		self->client->v_angle[YAW] -= spread;
		drop = Drop_Item (self, item);
		self->client->v_angle[YAW] += spread;
		drop->spawnflags = DROPPED_PLAYER_ITEM;
	}

	if (quad)
	{
		self->client->v_angle[YAW] += spread;
		drop = Drop_Item (self, FindItemByClassname ("item_quad"));
		self->client->v_angle[YAW] -= spread;
		drop->spawnflags |= DROPPED_PLAYER_ITEM;

		drop->touch = Touch_Item;
		drop->nextthink = level.time + (self->client->quad_framenum - level.framenum) * FRAMETIME;
		self->client->quad_framenum = 0;
		drop->think = G_FreeEdict;
	}

	//t-mek drop powershield, might be interesting
	/*if (//has powershiled)
	{
		self->client->v_angle[YAW] += spread;
		drop = Drop_Item (self, FindItemByClassname ("item_power_screen"));
		self->client->v_angle[YAW] -= spread;
		drop->spawnflags |= DROPPED_PLAYER_ITEM;

		drop->touch = Touch_Item;
		drop->nextthink = level.time + (self->client->quad_framenum - level.framenum) * FRAMETIME;
		drop->think = G_FreeEdict;
	}*/

}

/*
==================
ShowKiller -- JSW
==================
*/
void ShowKiller (edict_t *self, edict_t *attacker)
{
	// Make sure ent exists!
	if (!G_EntExists(self))
		return;
	if (!G_EntExists(attacker))
		return;
	if (attacker && attacker != world && attacker != self)
		gi.centerprintf(self, "You were killed by %s", attacker->client->pers.netname);
}

/*
==================
LookAtKiller
==================
*/
void LookAtKiller (edict_t *self, edict_t *inflictor, edict_t *attacker)
{
	vec3_t		dir;

	// Make sure ent exists!
	if (!G_EntExists(self))
		return;

	
	if (attacker && attacker != world && attacker != self)
	{
		VectorSubtract (attacker->s.origin, self->s.origin, dir);
	}
	else if (inflictor && inflictor != world && inflictor != self)
	{
		VectorSubtract (inflictor->s.origin, self->s.origin, dir);
	}
	else
	{
		self->client->killer_yaw = self->s.angles[YAW];
		return;
	}

	self->client->killer_yaw = 180/M_PI*atan2(dir[1], dir[0]);
}

/*
==================
player_die
==================
*/
void player_die (edict_t *self, edict_t *inflictor, edict_t *attacker, int damage, vec3_t point)
{
	int		n;

	// Make sure ent exists!
	if (!G_EntExists(self))
		return;

	VectorClear (self->avelocity);

	self->takedamage = DAMAGE_YES;
	self->movetype = MOVETYPE_TOSS;

	self->s.modelindex2 = 0;	// remove linked weapon model
//ZOID
	self->s.modelindex3 = 0;	// remove linked ctf flag
//ZOID

	self->s.angles[0] = 0;
	self->s.angles[2] = 0;

	self->s.sound = 0;
	self->client->weapon_sound = 0;

	self->maxs[2] = -8;

//	self->solid = SOLID_NOT;
	self->svflags |= SVF_DEADMONSTER;

	if (!self->deadflag)
	{
		self->client->respawn_time = level.time + 1.0;
		LookAtKiller (self, inflictor, attacker);
		ShowKiller (self, attacker);	//Show the client who killed them
		self->client->ps.pmove.pm_type = PM_DEAD;
		ClientObituary (self, inflictor, attacker);
//ZOID
		CTFFragBonuses(self, inflictor, attacker);
//ZOID
        /* SL - Log Player Score */
        /* GS - Use Generic Function */
        sl_WriteStdLogDeath( &gi, level, self, inflictor, attacker);


		TossClientWeapon (self);
//ZOID
		CTFPlayerResetGrapple(self);
		CTFDeadDropFlag(self);
		CTFDeadDropTech(self);
//ZOID

//t-mek:  prevent overflows?
//		if (deathmatch->value && !self->client->showscores)
//			Cmd_Help_f (self);		// show scores
//t-mek:  prevent overflows?

	}
	
	// remove powerups
	self->client->quad_framenum = 0;
	self->client->invincible_framenum = 0;
	self->client->breather_framenum = 0;
	self->client->enviro_framenum = 0;

	// clear inventory
	memset(self->client->pers.inventory, 0, sizeof(self->client->pers.inventory));
		
	if (self->health < -40)
	{	// gib
		gi.sound (self, CHAN_BODY, gi.soundindex ("misc/udeath.wav"), 1, ATTN_NORM, 0);
		for (n= 0; n < 4; n++)
			ThrowGib (self, "models/objects/gibs/sm_meat/tris.md2", damage, GIB_ORGANIC);
		ThrowClientHead (self, damage);
//ZOID
		self->client->anim_priority = ANIM_DEATH;
		self->client->anim_end = 0;
//ZOID
		self->takedamage = DAMAGE_NO;
	}
	else
	{	// normal death
		if (!self->deadflag)
		{
			static int i;

			i = (i+1)%3;
			// start a death animation
			self->client->anim_priority = ANIM_DEATH;
			if (self->client->ps.pmove.pm_flags & PMF_DUCKED)
			{
				self->s.frame = FRAME_crdeath1-1;
				self->client->anim_end = FRAME_crdeath5;
			}
			else switch (i)
			{
			case 0:
				self->s.frame = FRAME_death101-1;
				self->client->anim_end = FRAME_death106;
				break;
			case 1:
				self->s.frame = FRAME_death201-1;
				self->client->anim_end = FRAME_death206;
				break;
			case 2:
				self->s.frame = FRAME_death301-1;
				self->client->anim_end = FRAME_death308;
				break;
			}
			gi.sound (self, CHAN_VOICE, gi.soundindex(va("*death%i.wav", (rand()%4)+1)), 1, ATTN_NORM, 0);
		}
	}

	self->deadflag = DEAD_DEAD;

	gi.linkentity (self);
}

//=======================================================================

/*
==============
InitClientPersistant

This is only called when the game first initializes in single player,
but is called after each death and level change in deathmatch
==============
*/
void InitClientPersistant (gclient_t *client)
{
	gitem_t		*item;

	memset (&client->pers, 0, sizeof(client->pers));

	item = FindItem("Blaster");
	client->pers.selected_item = ITEM_INDEX(item);
	client->pers.inventory[client->pers.selected_item] = 1;

	client->pers.weapon = item;
//ZOID
	client->pers.lastweapon = item;
//ZOID

//ZOID
	item = FindItem("Grapple");
	client->pers.inventory[ITEM_INDEX(item)] = 1;
	item = FindItem("Spam Cannon");
	client->pers.inventory[ITEM_INDEX(item)] = 1;
//ZOID

	client->pers.health			= 100;
	client->pers.max_health		= 100;

	client->pers.max_bullets	= 200;
	client->pers.max_shells		= 100;
	client->pers.max_rockets	= 50;
	client->pers.max_grenades	= 50;
	client->pers.max_cells		= 200;
	client->pers.max_slugs		= 50;

	client->pers.connected = true;
}


void InitClientResp (gclient_t *client)
{
//ZOID
	int ctf_team = client->resp.ctf_team;
//ZOID

//T-MEK
	qboolean id_state = client->resp.id_state;
	//int midnightvote = client->resp.midnightvote;
//T-MEK	
	
	memset (&client->resp, 0, sizeof(client->resp));
	
//ZOID
	client->resp.ctf_team = ctf_team;
//ZOID

//T-MEK
	client->resp.id_state = id_state;
	//client->resp.midnightvote = midnightvote;
//T-MEK
	
	client->resp.enterframe = level.framenum;
	client->resp.coop_respawn = client->pers;
 
//ZOID
	if (ctf->value && client->resp.ctf_team < CTF_TEAM1)
		CTFAssignTeam(client);
//ZOID
}

/*
==================
SaveClientData

Some information that should be persistant, like health, 
is still stored in the edict structure, so it needs to
be mirrored out to the client structure before all the
edicts are wiped.
==================
*/
void SaveClientData (void)
{
	int		i;
	edict_t	*ent;

	for (i=0 ; i<game.maxclients ; i++)
	{
		ent = &g_edicts[1+i];
		if (!ent->inuse)
			continue;
		game.clients[i].pers.health = ent->health;
		game.clients[i].pers.max_health = ent->max_health;
		game.clients[i].pers.powerArmorActive = (ent->flags & FL_POWER_ARMOR);
		if (coop->value)
			game.clients[i].pers.score = ent->client->resp.score;
	}
}

void FetchClientEntData (edict_t *ent)
{
	// Make sure ent exists!
	if (!G_EntExists(ent))
		return;

	ent->health = ent->client->pers.health;
	ent->max_health = ent->client->pers.max_health;
	if (ent->client->pers.powerArmorActive)
		ent->flags |= FL_POWER_ARMOR;
	if (coop->value)
		ent->client->resp.score = ent->client->pers.score;
}



/*
=======================================================================

  SelectSpawnPoint

=======================================================================
*/

/*
================
PlayersRangeFromSpot

Returns the distance to the nearest player from the given spot
================
*/
float	PlayersRangeFromSpot (edict_t *spot)
{
	edict_t	*player;
	float	bestplayerdistance;
	vec3_t	v;
	int		n;
	float	playerdistance;


	bestplayerdistance = 9999999;

	for (n = 1; n <= maxclients->value; n++)
	{
		player = &g_edicts[n];

		if (!player->inuse)
			continue;

		if (player->health <= 0)
			continue;

		VectorSubtract (spot->s.origin, player->s.origin, v);
		playerdistance = VectorLength (v);

		if (playerdistance < bestplayerdistance)
			bestplayerdistance = playerdistance;
	}

	return bestplayerdistance;
}

/*
================
SelectRandomDeathmatchSpawnPoint

go to a random point, but NOT the two points closest
to other players
================
*/
edict_t *SelectRandomDeathmatchSpawnPoint (void)
{
	edict_t	*spot, *spot1, *spot2;
	int		count = 0;
	int		selection;
	float	range, range1, range2;

	spot = NULL;
	range1 = range2 = 99999;
	spot1 = spot2 = NULL;

	while ((spot = G_Find (spot, FOFS(classname), "info_player_deathmatch")) != NULL)
	{
		count++;
		range = PlayersRangeFromSpot(spot);
		if (range < range1)
		{
			range1 = range;
			spot1 = spot;
		}
		else if (range < range2)
		{
			range2 = range;
			spot2 = spot;
		}
	}

	if (!count)
		return NULL;

	if (count <= 2)
	{
		spot1 = spot2 = NULL;
	}
	else
		count -= 2;

	selection = rand() % count;

	spot = NULL;
	do
	{
		spot = G_Find (spot, FOFS(classname), "info_player_deathmatch");
		if (spot == spot1 || spot == spot2)
			selection++;
	} while(selection--);

	return spot;
}

/*
================
SelectFarthestDeathmatchSpawnPoint

================
*/
edict_t *SelectFarthestDeathmatchSpawnPoint (void)
{
	edict_t	*bestspot;
	float	bestdistance, bestplayerdistance;
	edict_t	*spot;


	spot = NULL;
	bestspot = NULL;
	bestdistance = 0;
	while ((spot = G_Find (spot, FOFS(classname), "info_player_deathmatch")) != NULL)
	{
		bestplayerdistance = PlayersRangeFromSpot (spot);

		if (bestplayerdistance > bestdistance)
		{
			bestspot = spot;
			bestdistance = bestplayerdistance;
		}
	}

	if (bestspot)
	{
		return bestspot;
	}

	// if there is a player just spawned on each and every start spot
	// we have no choice to turn one into a telefrag meltdown
	spot = G_Find (NULL, FOFS(classname), "info_player_deathmatch");

	return spot;
}

edict_t *SelectDeathmatchSpawnPoint (void)
{
	if ( (int)(dmflags->value) & DF_SPAWN_FARTHEST)
		return SelectFarthestDeathmatchSpawnPoint ();
	else
		return SelectRandomDeathmatchSpawnPoint ();
}


edict_t *SelectCoopSpawnPoint (edict_t *ent)
{
	int		index;
	edict_t	*spot = NULL;
	char	*target;

	index = ent->client - game.clients;

	// player 0 starts in normal player spawn point
	if (!index)
		return NULL;

	spot = NULL;

	// assume there are four coop spots at each spawnpoint
	while (1)
	{
		spot = G_Find (spot, FOFS(classname), "info_player_coop");
		if (!spot)
			return NULL;	// we didn't have enough...

		target = spot->targetname;
		if (!target)
			target = "";
		if ( Q_stricmp(game.spawnpoint, target) == 0 )
		{	// this is a coop spawn point for one of the clients here
			index--;
			if (!index)
				return spot;		// this is it
		}
	}


	return spot;
}


/*
===========
SelectSpawnPoint

Chooses a player start, deathmatch start, coop start, etc
============
*/
void	SelectSpawnPoint (edict_t *ent, vec3_t origin, vec3_t angles)
{
	edict_t	*spot = NULL;

	if (deathmatch->value)
//ZOID
		if (ctf->value)
			spot = SelectCTFSpawnPoint(ent);
		else
//ZOID
			spot = SelectDeathmatchSpawnPoint ();
	else if (coop->value)
		spot = SelectCoopSpawnPoint (ent);

	// find a single player start spot
	if (!spot)
	{
		while ((spot = G_Find (spot, FOFS(classname), "info_player_start")) != NULL)
		{
			if (!game.spawnpoint[0] && !spot->targetname)
				break;

			if (!game.spawnpoint[0] || !spot->targetname)
				continue;

			if (Q_stricmp(game.spawnpoint, spot->targetname) == 0)
				break;
		}

		if (!spot)
		{
			if (!game.spawnpoint[0])
			{	// there wasn't a spawnpoint without a target, so use any
				spot = G_Find (spot, FOFS(classname), "info_player_start");
			}
			if (!spot)
				gi.error ("Couldn't find spawn point %s\n", game.spawnpoint);
		}
	}

	VectorCopy (spot->s.origin, origin);
	origin[2] += 9;
	VectorCopy (spot->s.angles, angles);
}

//======================================================================


void InitBodyQue (void)
{
	int		i;
	edict_t	*ent;

	level.body_que = 0;
	for (i=0; i<BODY_QUEUE_SIZE ; i++)
	{
		ent = G_Spawn();
		ent->classname = "bodyque";
	}
}

void body_die (edict_t *self, edict_t *inflictor, edict_t *attacker, int damage, vec3_t point)
{
	int	n;

	if (self->health < -40)
	{
		gi.sound (self, CHAN_BODY, gi.soundindex ("misc/udeath.wav"), 1, ATTN_NORM, 0);
		for (n= 0; n < 4; n++)
			ThrowGib (self, "models/objects/gibs/sm_meat/tris.md2", damage, GIB_ORGANIC);
		self->s.origin[2] -= 48;
		ThrowClientHead (self, damage);
		self->takedamage = DAMAGE_NO;
	}
}

void CopyToBodyQue (edict_t *ent)
{
	edict_t		*body;


	// grab a body que and cycle to the next one
	body = &g_edicts[(int)maxclients->value + level.body_que + 1];
	level.body_que = (level.body_que + 1) % BODY_QUEUE_SIZE;

	// FIXME: send an effect on the removed body

	gi.unlinkentity (ent);

	gi.unlinkentity (body);
	body->s = ent->s;
	body->s.number = body - g_edicts;

	body->svflags = ent->svflags;
	VectorCopy (ent->mins, body->mins);
	VectorCopy (ent->maxs, body->maxs);
	VectorCopy (ent->absmin, body->absmin);
	VectorCopy (ent->absmax, body->absmax);
	VectorCopy (ent->size, body->size);
	body->solid = ent->solid;
	body->clipmask = ent->clipmask;
	body->owner = ent->owner;
	body->movetype = ent->movetype;

	body->die = body_die;
	body->takedamage = DAMAGE_YES;

	gi.linkentity (body);
}


void respawn (edict_t *self)
{
	if (deathmatch->value || coop->value)
	{
		CopyToBodyQue (self);
		PutClientInServer (self);

		// add a teleportation effect
		self->s.event = EV_PLAYER_TELEPORT;
		self->client->invincible_framenum = level.framenum + (respawn_time->value * 10);
		// hold in place briefly
		//t-mek // (only do on players who are real not before game starts)
		if (self->movetype != MOVETYPE_NOCLIP)
		{
			self->client->ps.pmove.pm_flags = PMF_TIME_TELEPORT;
			self->client->ps.pmove.pm_time = 14;
		}
		//t-mek

		self->client->respawn_time = level.time;

		return;
	}

	// restart the entire server
	gi.AddCommandString ("menu_loadgame\n");
}

//==============================================================

//RAVEN
void ShowGun(edict_t *ent);
//

/*
===========
PutClientInServer

Called when a player connects to a server or respawns in
a deathmatch.
============
*/
void PutClientInServer (edict_t *ent)
{
	vec3_t	mins = {-16, -16, -24};
	vec3_t	maxs = {16, 16, 32};
	int		index;
	vec3_t	spawn_origin, spawn_angles;
	gclient_t	*client;
	int		i;
	client_persistant_t	saved;
	client_respawn_t	resp;


	//T-MEK REVERT
//	gi.bprintf (PRINT_HIGH, "%s called put client in server\n", ent->client->pers.netname);
	//t-mek REVERT!

	// find a spawn point
	// do it before setting health back up, so farthest
	// ranging doesn't count this client
	SelectSpawnPoint (ent, spawn_origin, spawn_angles);

	index = ent-g_edicts-1;
	client = ent->client;

	// deathmatch wipes most client data every spawn
	if (deathmatch->value)
	{
		char		userinfo[MAX_INFO_STRING];

		resp = client->resp;
		memcpy (userinfo, client->pers.userinfo, sizeof(userinfo));
		InitClientPersistant (client);
		ClientUserinfoChanged (ent, userinfo);
	}
	else if (coop->value)
	{
		int			n;
		char		userinfo[MAX_INFO_STRING];

		resp = client->resp;
		memcpy (userinfo, client->pers.userinfo, sizeof(userinfo));
		// this is kind of ugly, but it's how we want to handle keys in coop
		for (n = 0; n < MAX_ITEMS; n++)
		{
			if (itemlist[n].flags & IT_KEY)
				resp.coop_respawn.inventory[n] = client->pers.inventory[n];
		}
		client->pers = resp.coop_respawn;
		ClientUserinfoChanged (ent, userinfo);
		if (resp.score > client->pers.score)
			client->pers.score = resp.score;
	}
	else
	{
		memset (&resp, 0, sizeof(resp));
	}

	// clear everything but the persistant data
	saved = client->pers;
	memset (client, 0, sizeof(*client));
	client->pers = saved;
	if (client->pers.health <= 0)
		InitClientPersistant(client);
	client->resp = resp;

	//t-mek
	ent->client->resp.inplayerque = 0.0; //reset this to false when a player joins a team
	//t-mek

	// copy some data from the client to the entity
	FetchClientEntData (ent);

	// clear entity values
	ent->groundentity = NULL;
	ent->client = &game.clients[index];
	ent->takedamage = DAMAGE_AIM;
	ent->movetype = MOVETYPE_WALK;
	ent->viewheight = 22;
	ent->inuse = true;
	ent->classname = "player";
	ent->mass = 200;
	ent->solid = SOLID_BBOX;
	ent->deadflag = DEAD_NO;
	ent->air_finished = level.time + 12;
	ent->clipmask = MASK_PLAYERSOLID;
	ent->model = "players/male/tris.md2";
	ent->pain = player_pain;
	ent->die = player_die;
	ent->waterlevel = 0;
	ent->watertype = 0;
	ent->flags &= ~FL_NO_KNOCKBACK;
	ent->svflags &= ~SVF_DEADMONSTER;

	VectorCopy (mins, ent->mins);
	VectorCopy (maxs, ent->maxs);
	VectorClear (ent->velocity);

	// clear playerstate values
	memset (&ent->client->ps, 0, sizeof(client->ps));

	client->ps.pmove.origin[0] = spawn_origin[0]*8;
	client->ps.pmove.origin[1] = spawn_origin[1]*8;
	client->ps.pmove.origin[2] = spawn_origin[2]*8;
//ZOID
	client->ps.pmove.pm_flags &= ~PMF_NO_PREDICTION;
//ZOID

	if (deathmatch->value && ((int)dmflags->value & DF_FIXED_FOV))
	{
		client->ps.fov = 90;
	}
	else
	{
		client->ps.fov = atoi(Info_ValueForKey(client->pers.userinfo, "fov"));
		if (client->ps.fov < 1)
			client->ps.fov = 90;
		else if (client->ps.fov > 160)
			client->ps.fov = 160;
	}

	client->ps.gunindex = gi.modelindex(client->pers.weapon->view_model);

	// clear entity state values
	ent->s.effects = 0;
	ent->s.skinnum = ent - g_edicts - 1;
	ent->s.modelindex = 255;		// will use the skin specified model
	//RAVEN
	//ent->s.modelindex2 = 255;		// custom gun model
	ShowGun(ent);
	//
	ent->s.frame = 0;
	VectorCopy (spawn_origin, ent->s.origin);
	ent->s.origin[2] += 1;	// make sure off ground
	VectorCopy (ent->s.origin, ent->s.old_origin);

	// set the delta angle
	for (i=0 ; i<3 ; i++)
		client->ps.pmove.delta_angles[i] = ANGLE2SHORT(spawn_angles[i] - client->resp.cmd_angles[i]);

	ent->s.angles[PITCH] = 0;
	ent->s.angles[YAW] = spawn_angles[YAW];
	ent->s.angles[ROLL] = 0;
	VectorCopy (ent->s.angles, client->ps.viewangles);
	VectorCopy (ent->s.angles, client->v_angle);

//ZOID
	if (CTFStartClient(ent))
		return;
//ZOID

	//t-mek testing!!!
		// start as 'observer'

	if(level.time < (matchdelay->value-.5)){  //allowplay  matchtime
		ent->movetype = MOVETYPE_NOCLIP;
		ent->solid = SOLID_NOT;
//		ent->svflags |= SVF_NOCLIENT;
		//ent->client->resp.ctf_team = CTF_NOTEAM;
	
		//t-mek: visible spectators
		ent->s.modelindex = gi.modelindex ("models/items/c_head/tris.md2");
		ent->svflags &= ~SVF_NOCLIENT;  //T-MEK MOD (send to clients. ie. make visible)
		ent->s.renderfx = RF_TRANSLUCENT;  //slightly transparent
		ent->s.modelindex2 = 0;	// remove linked weapon model
		ent->s.modelindex3 = 0;	// remove linked ctf flag
		ent->s.skinnum = 0;
		ent->s.frame = 0;
		//t-mek
		
		ent->client->ps.gunindex = 0;
		return;
	}

	//t-mek testing!!!
	

	if (!KillBox (ent))
	{	// could't spawn in?
	}

	gi.linkentity (ent);

	// force the current weapon up
	client->newweapon = client->pers.weapon;
	ChangeWeapon (ent);
}

/*
=====================
ClientBeginDeathmatch

A client has just connected to the server in 
deathmatch mode, so clear everything out before starting them.
=====================
*/
void ClientBeginDeathmatch (edict_t *ent)
{
	// STEVE added these 3 local variables
	FILE *motd_file;
	char motd[500];
	char line[80];
	int i;
	char filename[80];

	G_InitEdict (ent);

	InitClientResp (ent->client);

	ent->client->midnight_vote = 1;

	// locate ent at a spawn point
	PutClientInServer (ent);

	// send effect
	gi.WriteByte (svc_muzzleflash);
	gi.WriteShort (ent-g_edicts);
	gi.WriteByte (MZ_LOGIN);
	gi.multicast (ent->s.origin, MULTICAST_PVS);

	gi.bprintf (PRINT_HIGH, "%s entered the game\n", ent->client->pers.netname);

	// STEVE changed this bit : read the motd from a file
	
	i =  sprintf(filename, "./");
	i += sprintf(filename + i, gamedir->string);
	i += sprintf(filename + i, "/motd.txt", level.mapname);

	if (motd_file = fopen(filename, "r"))
	{
		// we successfully opened the file "motd.txt"
		if ( fgets(motd, 500, motd_file) )
		{
			// we successfully read a line from "motd.txt" into motd
			// ... read the remaining lines now
			while ( fgets(line, 80, motd_file) )
			{
				// add each new line to motd, to create a BIG message string.
				// we are using strcat: STRing conCATenation function here.
				strcat(motd, line);
			}

			// print our message.
			gi.centerprintf (ent, motd);
		}
		// be good now ! ... close the file
		fclose(motd_file);
	}
	

	// make sure all view stuff is valid
	ClientEndServerFrame (ent);
}


/*
===========
ClientBegin

called when a client has finished connecting, and is ready
to be placed into the game.  This will happen every level load.
============
*/
void ClientBegin (edict_t *ent)
{
	int		i;

	ent->client = game.clients + (ent - g_edicts - 1);

	if (deathmatch->value)
	{
		ClientBeginDeathmatch (ent);
		return;
	}

	// if there is already a body waiting for us (a loadgame), just
	// take it, otherwise spawn one from scratch
	if (ent->inuse == true)
	{
		// the client has cleared the client side viewangles upon
		// connecting to the server, which is different than the
		// state when the game is saved, so we need to compensate
		// with deltaangles
		for (i=0 ; i<3 ; i++)
			ent->client->ps.pmove.delta_angles[i] = ANGLE2SHORT(ent->client->ps.viewangles[i]);
	}
	else
	{
		// a spawn point will completely reinitialize the entity
		// except for the persistant data that was initialized at
		// ClientConnect() time
		G_InitEdict (ent);
		ent->classname = "player";
		InitClientResp (ent->client);
		PutClientInServer (ent);
	}

	if (level.intermissiontime)
	{
		MoveClientToIntermission (ent);
	}
	else
	{
		// send effect if in a multiplayer game
		if (game.maxclients > 1)
		{
			gi.WriteByte (svc_muzzleflash);
			gi.WriteShort (ent-g_edicts);
			gi.WriteByte (MZ_LOGIN);
			gi.multicast (ent->s.origin, MULTICAST_PVS);

			gi.bprintf (PRINT_HIGH, "%s entered the game\n", ent->client->pers.netname);
		}
	}

	// make sure all view stuff is valid
	ClientEndServerFrame (ent);
}

/*
===========
ClientUserInfoChanged

called whenever the player updates a userinfo variable.

The game can override any of the settings in place
(forcing skins or names, etc) before copying it off.
============
*/
void ClientUserinfoChanged (edict_t *ent, char *userinfo)
{
	char	*s;
	int		playernum;
    int     fIgnoreName = 0;  /* SL - Player Name Clash Check Variable */

	// check for malformed or illegal info strings
	if (!Info_Validate(userinfo))
	{
		strcpy (userinfo, "\\name\\badinfo\\skin\\male/grunt");
	}

    /* SL - BEGIN - Check if player name is not empty */
    /* SL -       - Ignore name changes */
#if 0
    if( '\0' != ent->client->pers.netname[0] )
    {
        Info_SetValueForKey (userinfo, "name", ent->client->pers.netname);
        fIgnoreName = 1;
    }
#endif
    /* SL -  END  - Check if player name is not empty */

    // set name
	s = Info_ValueForKey (userinfo, "name");
	if ( Q_stricmp(s, "console") == 0)
		s = ent->client->pers.netname;
	if (Q_stricmp(s, "")==0)
		sprintf(s, "noname");
    /* SL - BEGIN - Player Name Clash Check */
    if( !fIgnoreName )
    {
        edict_t		*cl_ent;
        unsigned int i;

        for (i=0 ; i<game.maxclients ; i++)
        {
            cl_ent = g_edicts + 1 + i;
            if( cl_ent->inuse &&
                (cl_ent != ent) &&
                !strcmp(cl_ent->client->pers.netname, s) )
            {
                //gi.cprintf(ent, PRINT_HIGH, "Your name clashes with another player's.\n");
                Info_SetValueForKey(userinfo, "rejmsg", "Your name clashes with another player's.");

                if( '\0' != ent->client->pers.netname[0] )
                {
                    fIgnoreName = 1;
					s = ent->client->pers.netname;
                    gi.cprintf(ent, PRINT_HIGH, "Could not change name. Original name kept.\n");
                }
                else
                {
                    s = ent->client->pers.netname;
                    gi.cprintf(ent, PRINT_HIGH, "Could not set name. Please pick another name.\n");
                }


                /* Put client into spectator mode */
				//stuffcmd(ent, "team spec");
				Info_SetValueForKey (userinfo, "name", va("%s", ent->client->pers.netname));
                break;
            }
        }
    }
    /* SL -  END  - Player Name Clash Check */

    /* SL - BEGIN - Player Name Set */
	if( !fIgnoreName )
    {
        /* Has the player got a name */
        if( strlen(ent->client->pers.netname) )
        {
            /* Has the name changed */
            if( strcmp( ent->client->pers.netname, s ) )
            {
                /* SL - log player rename */
                sl_LogPlayerRename( &gi,
                                    ent->client->pers.netname,
                                    s,
                                    level.time );
            }
        }

		strncpy (ent->client->pers.netname, s, sizeof(ent->client->pers.netname)-1);
    }
    /* SL -  END  - Player Name Set */

	// set skin
	s = Info_ValueForKey (userinfo, "skin");

	playernum = ent-g_edicts-1;

	// combine name and skin into a configstring
//ZOID
	if (ctf->value)
		CTFAssignSkin(ent, s);
	else
//ZOID
		gi.configstring (CS_PLAYERSKINS+playernum, va("%s\\%s", ent->client->pers.netname, s) );

//T-MEK
	gi.configstring (CS_PLAYERNAMES+playernum, va("%s", ent->client->pers.netname) );
	gi.configstring (CS_PLAYERNAMESRIGHT+playernum, va("%15s", ent->client->pers.netname) );
//T-MEK

	// fov
	if (deathmatch->value && ((int)dmflags->value & DF_FIXED_FOV))
	{
		ent->client->ps.fov = 90;
	}
	else
	{
		ent->client->ps.fov = atoi(Info_ValueForKey(userinfo, "fov"));
		if (ent->client->ps.fov < 1)
			ent->client->ps.fov = 90;
		else if (ent->client->ps.fov > 160)
			ent->client->ps.fov = 160;
	}

	// handedness
	s = Info_ValueForKey (userinfo, "hand");
	if (strlen(s))
	{
		ent->client->pers.hand = atoi(s);
	}

	// save off the userinfo in case we want to check something later
	strncpy (ent->client->pers.userinfo, userinfo, sizeof(ent->client->pers.userinfo)-1);

}


/*
===========
ClientConnect

Called when a player begins connecting to the server.
The game can refuse entrance to a client by returning false.
If the client is allowed, the connection process will continue
and eventually get to ClientBegin()
Changing levels will NOT cause this to be called again, but
loadgames will.
============
*/
qboolean ClientConnect (edict_t *ent, char *userinfo)
{
	char	*value;
	char	*ip;

	// check to see if they are on the banned IP list
	value = Info_ValueForKey (userinfo, "ip");

	// check for a password
	value = Info_ValueForKey (userinfo, "password");
	if (strcmp(password->string, value) != 0)
		return false;

	// they can connect
	ent->client = game.clients + (ent - g_edicts - 1);

	// if there is already a body waiting for us (a loadgame), just
	// take it, otherwise spawn one from scratch
	if (ent->inuse == false)
	{
		// clear the respawning variables
//ZOID -- force team join
		ent->client->resp.ctf_team = -1;
//ZOID
		InitClientResp (ent->client);
		if (!game.autosaved || !ent->client->pers.weapon)
			InitClientPersistant (ent->client);
	}

	ClientUserinfoChanged (ent, userinfo);

	/* SL - Kick player if reject message set */
    if( Info_ValueForKey(userinfo, "rejmsg")[0] != '\0' )
        return false;

	
	ip = Info_ValueForKey (ent->client->pers.userinfo, "ip");
	if (game.maxclients > 1)
		gi.dprintf ("%s connected from %s\n", ent->client->pers.netname, ip);

	/* SL - Log Player Connect */
    sl_LogPlayerConnect( &gi,
                         ent->client->pers.netname,
                         NULL,
                         level.time );

	ent->client->pers.connected = true;
	return true;
}

/*
===========
ClientDisconnect

Called when a player drops from the server.
Will not be called between levels.
============
*/
void ClientDisconnect (edict_t *ent)
{
	int		playernum;

	// Make sure ent exists!
	if (!G_EntExists(ent))
		return;

	TossClientWeapon(ent);

	gi.bprintf (PRINT_HIGH, "%s disconnected\n", ent->client->pers.netname);

    /* SL - Log Player Disconnection */
    sl_LogPlayerLeft( &gi,
                      ent->client->pers.netname,
                      level.time );
	
//ZOID
	CTFDeadDropFlag(ent);
	CTFDeadDropTech(ent);
//ZOID

	// send effect
	gi.WriteByte (svc_muzzleflash);
	gi.WriteShort (ent-g_edicts);
	gi.WriteByte (MZ_LOGOUT);
	gi.multicast (ent->s.origin, MULTICAST_PVS);

	gi.unlinkentity (ent);
	ent->s.modelindex = 0;
	ent->solid = SOLID_NOT;
	ent->inuse = false;
	ent->classname = "disconnected";
	ent->client->pers.connected = false;

	playernum = ent-g_edicts-1;
	gi.configstring (CS_PLAYERSKINS+playernum, "");

	//T-MEK
	gi.configstring (CS_PLAYERNAMES+playernum, "");
	gi.configstring (CS_PLAYERNAMESRIGHT+playernum, "");
	//T-MEK
	//JSW
	ent->client->midnight_vote = 0;
	CheckMidnight();	//make sure the lights reflect the votes
}


//==============================================================


edict_t	*pm_passent;

// pmove doesn't need to know about passent and contentmask
trace_t	PM_trace (vec3_t start, vec3_t mins, vec3_t maxs, vec3_t end)
{
	if (pm_passent->health > 0)
		return gi.trace (start, mins, maxs, end, pm_passent, MASK_PLAYERSOLID);
	else
		return gi.trace (start, mins, maxs, end, pm_passent, MASK_DEADSOLID);
}

unsigned CheckBlock (void *b, int c)
{
	int	v,i;
	v = 0;
	for (i=0 ; i<c ; i++)
		v+= ((byte *)b)[i];
	return v;
}
void PrintPmove (pmove_t *pm)
{
	unsigned	c1, c2;

	c1 = CheckBlock (&pm->s, sizeof(pm->s));
	c2 = CheckBlock (&pm->cmd, sizeof(pm->cmd));
	Com_Printf ("sv %3i:%i %i\n", pm->cmd.impulse, c1, c2);
}

/*
==============
ClientThink

This will be called once for each client frame, which will
usually be a couple times for each server frame.
==============
*/
//RAVEN
void	PlaceLaser (edict_t *ent);
//

void ClientThink (edict_t *ent, usercmd_t *ucmd)
{
	gclient_t	*client;
	edict_t	*other;
	int		i, j;
	pmove_t	pm;
//	char	command [256]; //t-mek: for kicking zbots

	// Make sure ent exists!
	if (!G_EntExists(ent))
		return;

	level.current_entity = ent;
	client = ent->client;

	if (level.intermissiontime)
	{
		client->ps.pmove.pm_type = PM_FREEZE;
		// can exit intermission after five seconds
		if (level.time > level.intermissiontime + 8.0 //RAV (5) for Highscores 
			&& (ucmd->buttons & BUTTON_ANY) )
			level.exitintermission = true;
		return;
	}


	pm_passent = ent;

//ZOID
	if (ent->client->chase_target) {
		client->resp.cmd_angles[0] = SHORT2ANGLE(ucmd->angles[0]);
		client->resp.cmd_angles[1] = SHORT2ANGLE(ucmd->angles[1]);
		client->resp.cmd_angles[2] = SHORT2ANGLE(ucmd->angles[2]);
		return;
	}
//ZOID

//T-MEK
/*
//  RAVEN  this code is not found !!!
  if(ZbotCheck(ent,ucmd)){
		gi.bprintf(PRINT_HIGH,"------- Zbot Detected: %s -------\n",ent->client->pers.netname);
		Com_sprintf(command, sizeof(command), "kick %d", ent-g_edicts-1);
		gi.AddCommandString (command);
		gi.bprintf(PRINT_HIGH,"------- Zbot Detected: %s -------\n",ent->client->pers.netname);
	}
//T-MEK
*/
//RAVEN laser timer 
	if(ent->client->resp.lasertimer != 0 &&
		ent->client->resp.lasertimer == level.time)
	{
	PlaceLaser (ent);
	ent->client->resp.lasertimer = 0;
	}
//
	// set up for pmove
	memset (&pm, 0, sizeof(pm));

	if (ent->movetype == MOVETYPE_NOCLIP)
		client->ps.pmove.pm_type = PM_SPECTATOR;
	else if (ent->s.modelindex != 255)
		client->ps.pmove.pm_type = PM_GIB;
	else if (ent->deadflag)
		client->ps.pmove.pm_type = PM_DEAD;
	else
		client->ps.pmove.pm_type = PM_NORMAL;

	client->ps.pmove.gravity = sv_gravity->value;
	pm.s = client->ps.pmove;

	for (i=0 ; i<3 ; i++)
	{
		pm.s.origin[i] = ent->s.origin[i]*8;
		pm.s.velocity[i] = ent->velocity[i]*8;
	}

	if (memcmp(&client->old_pmove, &pm.s, sizeof(pm.s)))
	{
		pm.snapinitial = true;
//		gi.dprintf ("pmove changed!\n");
	}

	pm.cmd = *ucmd;

	pm.trace = PM_trace;	// adds default parms
	pm.pointcontents = gi.pointcontents;

	// perform a pmove
	gi.Pmove (&pm);

	// save results of pmove
	client->ps.pmove = pm.s;
	client->old_pmove = pm.s;

	for (i=0 ; i<3 ; i++)
	{
		ent->s.origin[i] = pm.s.origin[i]*0.125;
		ent->velocity[i] = pm.s.velocity[i]*0.125;
	}

	VectorCopy (pm.mins, ent->mins);
	VectorCopy (pm.maxs, ent->maxs);

	client->resp.cmd_angles[0] = SHORT2ANGLE(ucmd->angles[0]);
	client->resp.cmd_angles[1] = SHORT2ANGLE(ucmd->angles[1]);
	client->resp.cmd_angles[2] = SHORT2ANGLE(ucmd->angles[2]);

	if (ent->groundentity && !pm.groundentity && (pm.cmd.upmove >= 10) && (pm.waterlevel == 0))
	{
		gi.sound(ent, CHAN_VOICE, gi.soundindex("*jump1.wav"), 1, ATTN_NORM, 0);
		PlayerNoise(ent, ent->s.origin, PNOISE_SELF);
	}

	if (ent->movetype == MOVETYPE_NOCLIP)
		ent->viewheight = 11;  //t-mek  make floating heads eyes correct height
	else
		ent->viewheight = pm.viewheight;

	ent->waterlevel = pm.waterlevel;
	ent->watertype = pm.watertype;
	ent->groundentity = pm.groundentity;
	if (pm.groundentity)
		ent->groundentity_linkcount = pm.groundentity->linkcount;

	if (ent->deadflag)
	{
		client->ps.viewangles[ROLL] = 40;
		client->ps.viewangles[PITCH] = -15;
		client->ps.viewangles[YAW] = client->killer_yaw;
	}
	else
	{
		VectorCopy (pm.viewangles, client->v_angle);
		VectorCopy (pm.viewangles, client->ps.viewangles);
	}

//ZOID
	if (client->ctf_grapple)
		CTFGrapplePull(client->ctf_grapple);
//ZOID

	gi.linkentity (ent);

	if (ent->movetype != MOVETYPE_NOCLIP)
		G_TouchTriggers (ent);

	// touch other objects
	for (i=0 ; i<pm.numtouch ; i++)
	{
		other = pm.touchents[i];
		for (j=0 ; j<i ; j++)
			if (pm.touchents[j] == other)
				break;
		if (j != i)
			continue;	// duplicated
		if (!other->touch)
			continue;
		other->touch (other, ent, NULL, NULL);
	}


	client->oldbuttons = client->buttons;
	client->buttons = ucmd->buttons;
	client->latched_buttons |= client->buttons & ~client->oldbuttons;

	// save light level the player is standing on for
	// monster sighting AI
	ent->light_level = ucmd->lightlevel;

	// fire weapon from final position if needed
	if (client->latched_buttons & BUTTON_ATTACK
//ZOID
		&& ent->movetype != MOVETYPE_NOCLIP
//ZOID
		)
	{
		if (!client->weapon_thunk)
		{
			client->weapon_thunk = true;
			Think_Weapon (ent);
		}
	}

//ZOID
//regen tech
	CTFApplyRegeneration(ent);
//ZOID

//ZOID
	for (i = 1; i <= maxclients->value; i++) {
		other = g_edicts + i;
		if (other->inuse && other->client->chase_target == ent)
			UpdateChaseCam(other);
	}
//ZOID
}


/*
==============
ClientBeginServerFrame

This will be called once for each server frame, before running
any other entities in the world.
==============
*/
void ClientBeginServerFrame (edict_t *ent)
{
	gclient_t	*client;
	int			buttonMask;

	if (level.intermissiontime)
		return;

	client = ent->client;

	// run weapon animations if it hasn't been done by a ucmd_t
	if (!client->weapon_thunk
//ZOID
		&& ent->movetype != MOVETYPE_NOCLIP
//ZOID
		)
		Think_Weapon (ent);
	else
		client->weapon_thunk = false;

	if (ent->deadflag)
	{
		// wait for any button just going down
		if ( level.time > client->respawn_time)
		{
			// in deathmatch, only wait for attack button
			if (deathmatch->value)
				buttonMask = BUTTON_ATTACK;
			else
				buttonMask = -1;

			if ( ( client->latched_buttons & buttonMask ) ||
				(deathmatch->value && ((int)dmflags->value & DF_FORCE_RESPAWN) ) )
			{
				respawn(ent);
				client->latched_buttons = 0;
			}
		}
		return;
	}

	// add player trail so monsters can follow
	if (!deathmatch->value)
		if (!visible (ent, PlayerTrail_LastSpot() ) )
			PlayerTrail_Add (ent->s.old_origin);

	client->latched_buttons = 0;

}

