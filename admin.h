void ShowLevelTime ( edict_t *ent );
char *GameTime( void );
//void SetBiggerTeamStats(edict_t *ent );

qboolean G_EntExists(edict_t *ent);
qboolean G_ClientNotDead(edict_t *ent);
qboolean G_ClientInGame(edict_t *ent);
void SP_misc_phase_in_spawn_point (edict_t *ent);
void SP_misc_spawn_point_phase_out (edict_t *ent);
void G_Spawn_Sparks(int type, vec3_t start, vec3_t movdir, vec3_t origin );
edict_t *Find_Player_Edict_t (char *s);
