#include "g_local.h"
#include "lasrmine.h"
#include "admin.h"

void	PlaceLaser (edict_t *ent)
{
	char message[256];
	int cells;

	edict_t		*self,
				*grenade;

	vec3_t		forward,
				wallp;

	trace_t		tr;


/*	int		laser_colour[] = {
								0xf2f2f0f0,		// red
								0xd0d1d2d3,		// green
								0xf3f3f1f1,		// blue
								0xdcdddedf,		// yellow
								0xe0e1e2e3		// bitty yellow strobe
							};
*/
	int		laser_colour = 0xd0d1d2d3;

	// Make sure ent exists!
	if (!G_EntExists(ent))
		return;


	//test spectator
	if ( ent->client->resp.ctf_team == CTF_NOTEAM)
		return;
		
	//test ammo
	cells = ent->client->pers.inventory[ITEM_INDEX(FindItem("cells"))] ;
	
	if (cells < energylaser_cells->value )
	{
		cells = energylaser_cells->value - cells;
				
		if (cells < 0)
			cells = 0;
				
		strcpy (message, "You need ");

		if (cells != 0) //need cells
			sprintf(message,"%s%d more cells ",message,cells);
		
		gi.centerprintf(ent, "%s\n", message);
		return;
	}
	
	// Setup "little look" to close wall
	VectorCopy(ent->s.origin,wallp);         

	// Cast along view angle
	AngleVectors (ent->client->v_angle, forward, NULL, NULL);

	// Setup end point
	wallp[0]=ent->s.origin[0]+forward[0]*50;
	wallp[1]=ent->s.origin[1]+forward[1]*50;
	wallp[2]=ent->s.origin[2]+forward[2]*50;  

	// trace
	tr = gi.trace (ent->s.origin, NULL, NULL, wallp, ent, MASK_SOLID);

	// Line complete ? (ie. no collision)
	if (tr.fraction == 1.0)
	{
	 	gi.cprintf (ent, PRINT_HIGH, "Energy Lasers must be attached to a hard surface.\n");
		return;
	}

	// Hit sky ?
	if (tr.surface)
		if (tr.surface->flags & SURF_SKY)
			return;

	// Ok, lets stick one on then ...
	gi.centerprintf (ent, "Energy Laser attached.\n");

	ent->client->pers.inventory[ITEM_INDEX(FindItem("Cells"))] -= energylaser_cells->value;

// -----------
// Setup laser
// -----------
	self = G_Spawn();

	self -> movetype		= MOVETYPE_NONE;
	self -> solid			= SOLID_NOT;
	self -> s.renderfx		= RF_BEAM|RF_TRANSLUCENT;
	self -> s.modelindex	= 1;			// must be non-zero
	self -> s.sound			= gi.soundindex ("world/laser.wav");
	self -> classname		= "laser_yaya";
	self -> s.frame			= 2;	// beam diameter
  	self -> owner			= self;
	self -> s.skinnum		= laser_colour;
//	self -> s.skinnum		= laser_colour[((int) (random() * 1000)) % 5];
  	self -> dmg				= energylaser_damage->value;
	self -> think			= pre_target_laser_think;
	self -> delay			= level.time + energylaser_time->value;

	// Set orgin of laser to point of contact with wall
	VectorCopy(tr.endpos,self->s.origin);

	// convert normal at point of contact to laser angles
	vectoangles(tr.plane.normal,self -> s.angles);

	// setup laser movedir (projection of laser)
	G_SetMovedir (self->s.angles, self->movedir);

	VectorSet (self->mins, -8, -8, -8);
	VectorSet (self->maxs, 8, 8, 8);

	// link to world
	gi.linkentity (self);

	// start off ...
	target_laser_off (self);

	// ... but make automatically come on
	self -> nextthink = level.time + 2;

	grenade = G_Spawn();

	VectorClear (grenade->mins);
	VectorClear (grenade->maxs);
	VectorCopy (tr.endpos, grenade->s.origin);
	vectoangles(tr.plane.normal,grenade -> s.angles);

	grenade -> movetype		= MOVETYPE_NONE;
	grenade -> clipmask		= MASK_SHOT;
	grenade -> solid		= SOLID_NOT;
	grenade -> s.modelindex	= gi.modelindex ("models/objects/grenade2/tris.md2");
	grenade -> owner		= self;
	grenade -> nextthink	= level.time + energylaser_time->value;
	grenade -> think		= G_FreeEdict;

	gi.linkentity (grenade);
}

void target_lasermine_think (edict_t *self)
{
	edict_t	*ignore;
	vec3_t	start;
	vec3_t	end;
	trace_t	tr;
	vec3_t	point;
	vec3_t	last_movedir;
	int		count;

	if (strcmp(self -> classname,"laser_yaya") == 0)
		if (level.time > self -> delay)
		{
			// bit of damage
			T_RadiusDamage (self, self, energylaser_mradius->value, NULL, energylaser_mdamage->value, MOD_TARGET_LASER);

			// BANG !
			gi.WriteByte (svc_temp_entity);
			gi.WriteByte (TE_EXPLOSION1);
			gi.WritePosition(self -> s.origin);
			gi.multicast (self->s.origin, MULTICAST_PVS);

			// bye bye laser
			G_FreeEdict (self);

			return;
		}
	
	// t-mek  (free lasermine after 58 seconds)
	if (strcmp(self -> classname, "lasermine") == 0 && lasermine_timeout->value > 0)
		if (level.time > self->delay)
		{
			gi.sound (self, CHAN_WEAPON, gi.soundindex ("misc/spawn1.wav"), 1, ATTN_NORM,0);
			//target_laser_off (self);
			G_FreeEdict (self);
			return;
		}
	// t-mek
	
	
	if (self->spawnflags & 0x80000000)
		count = 8;
	else
		count = 4;

	if (self->enemy)
	{
		VectorCopy (self->movedir, last_movedir);
		VectorMA (self->enemy->absmin, 0.5, self->enemy->size, point);
		VectorSubtract (point, self->s.origin, self->movedir);
		VectorNormalize (self->movedir);
		if (!VectorCompare(self->movedir, last_movedir))
			self->spawnflags |= 0x80000000;
	}

	ignore = self;
	VectorCopy (self->s.origin, start);
	VectorMA (start, 2048, self->movedir, end);
	while(1)
	{
		tr = gi.trace (start, NULL, NULL, end, ignore, CONTENTS_SOLID|CONTENTS_MONSTER|CONTENTS_DEADMONSTER);

		if (!tr.ent)
			break;

//T-MEK
		//Allow *client entities only* to break the lasermine beams
		if (strcmp(self->classname, "lasermine") == 0)
			if (tr.ent->client) 
			{	
				if (Q_stricmp(self->owner->classname,"hgrenade")==0)  // don't explode grenade if it's already gone!
				{
					Grenade_Explode (self->owner);
				}
				G_FreeEdict (self);  
				return;
			}
//T-MEK

		// hurt it if we can
		if ((tr.ent->takedamage) && !(tr.ent->flags & FL_IMMUNE_LASER))
			T_Damage (tr.ent, self, self->activator, self->movedir, tr.endpos, vec3_origin, self->dmg, 1, DAMAGE_ENERGY, MOD_TARGET_LASER);

		// if we hit something that's not a monster or player or is immune to lasers, we're done
		if (!(tr.ent->svflags & SVF_MONSTER) && (!tr.ent->client))
		{
			if (self->spawnflags & 0x80000000)
			{
				self->spawnflags &= ~0x80000000;
				gi.WriteByte (svc_temp_entity);
				gi.WriteByte (TE_LASER_SPARKS);
				gi.WriteByte (count);
				gi.WritePosition (tr.endpos);
				gi.WriteDir (tr.plane.normal);
				gi.WriteByte (self->s.skinnum);
				gi.multicast (tr.endpos, MULTICAST_PVS);
			}
			break;
		}

		ignore = tr.ent;
		VectorCopy (tr.endpos, start);
	}

	VectorCopy (tr.endpos, self->s.old_origin);

	self->nextthink = level.time + FRAMETIME;
}




void pre_target_laser_think(edict_t *self)
{
		target_laser_on (self);

		self->think = target_lasermine_think;
}


void Place_LaserMine (edict_t *ent, cplane_t *plane, csurface_t *surf)
{
	edict_t *laser;
		
	//gi.centerprintf(ent->owner,"Placing %d laser mine.", ent->owner->client->resp.ctf_team);

	laser = G_Spawn();
	laser -> movetype		= MOVETYPE_NONE;
	laser -> solid			= SOLID_BBOX;
	laser -> s.renderfx		= RF_BEAM|RF_TRANSLUCENT;
	laser -> s.modelindex	= 1;
	laser -> s.sound		= gi.soundindex ("world/laser.wav");
	laser -> classname		= "lasermine";
	laser -> s.frame		= 2;
	laser -> owner			= ent;
	if (lasermine_timeout->value > 0 && lasermine_timeout->value < 4)
		lasermine_timeout->value = 4;
	laser -> delay			= level.time + (lasermine_timeout->value - 2);
	laser -> think			= pre_target_laser_think;
	
	// set laser color
	if (ent->owner->client->resp.ctf_team == 1) 
		laser -> s.skinnum		= 0xf2f2f0f0;  //red beam
	else
		laser -> s.skinnum		= 0xf3f3f1f1;  //red beam



	// position laser
	VectorCopy(ent -> s.origin, laser -> s.origin);
	// copy angle from plane to laser angle
	vectoangles(plane -> normal, laser -> s.angles);
	// setup laser movedir (projection of laser)
	G_SetMovedir (laser -> s.angles, laser->movedir);

	// do we need to set vectors? (yes we do i found out, but why?)

	//VectorSet (laser->mins, -8, -8, -8);
	//VectorSet (laser->maxs, 8, 8, 8);

	// link to world
	gi.linkentity (laser);
	target_laser_off (laser);
	laser -> nextthink = level.time + 2;

}



//This function was written to spawn a grenade for redteam base 
//on outlands to make things more fair on that map.
void spawn_outlandgrenade(void)
{
	edict_t *grenade;
	
	grenade = G_Spawn();
	
	VectorSet (grenade->s.origin, -3141, 1039, 43);
	grenade->classname = "ammo_grenades";

	//gi.dprintf("***EXTRA GRENADE SPAWNED***\n");

	ED_CallSpawn (grenade);
}
