
#include "g_local.h"
/* SL - Include Standard Logging Header File */
#include "stdlog.h"
#include "s_map.h"

qboolean MoreThanMaxPlayers(void);
int CTFSmallerTeam(void);
void CTFJoinTeam(edict_t *ent, int desired_team);

game_locals_t	game;
level_locals_t	level;
game_import_t	gi;
game_export_t	globals;
spawn_temp_t	st;

int	sm_meat_index;
int	snd_fry;
int meansOfDeath;

edict_t		*g_edicts;

cvar_t	*deathmatch;
cvar_t	*coop;
cvar_t	*dmflags;
cvar_t	*skill;
cvar_t	*fraglimit;
cvar_t	*timelimit;
//ZOID
cvar_t	*capturelimit;
//ZOID

//T-MEK revert!
cvar_t	*maxplayers;
cvar_t	*matchdelay;

cvar_t	*flood_msgs;
cvar_t  *flood_persecond;
cvar_t  *flood_waitdelay;
//T-MEK REVERT!



cvar_t	*password;
cvar_t	*maxclients;
cvar_t	*maxentities;
cvar_t	*g_select_empty;
cvar_t	*dedicated;

cvar_t	*sv_maxvelocity;
cvar_t	*sv_gravity;

cvar_t	*sv_rollspeed;
cvar_t	*sv_rollangle;
cvar_t	*gun_x;
cvar_t	*gun_y;
cvar_t	*gun_z;

cvar_t	*run_pitch;
cvar_t	*run_roll;
cvar_t	*bob_up;
cvar_t	*bob_pitch;
cvar_t	*bob_roll;

cvar_t	*sv_cheats;

//JSW
cvar_t	*lights;
cvar_t	*allow_midnight;
cvar_t	*grapple_speed;
cvar_t	*grapple_pull_speed;
cvar_t	*grapple_sky;
cvar_t	*respawn_time;
cvar_t	*allow_energy_lasers;
cvar_t	*energylaser_time;
cvar_t	*energylaser_damage;
cvar_t	*energylaser_cells;
cvar_t	*energylaser_mdamage;
cvar_t	*energylaser_mradius;
cvar_t	*welcomemsg;
cvar_t	*email;
cvar_t	*allownuke;
cvar_t	*nuke_cells;
cvar_t	*nuke_slugs;
cvar_t	*nuke_radius;
cvar_t	*nuke_radius2;
cvar_t	*console_chat;
cvar_t  *highscores;
cvar_t	*gamedir;
cvar_t	*sl_filename;
cvar_t	*map_c;
cvar_t	*map_r;
cvar_t	*map_o;
cvar_t	*map_d;
cvar_t	*lasermine_timeout;
cvar_t	*use_spam_cannon;
//JSW

void SpawnEntities (char *mapname, char *entities, char *spawnpoint);
void ClientThink (edict_t *ent, usercmd_t *cmd);
qboolean ClientConnect (edict_t *ent, char *userinfo);
void ClientUserinfoChanged (edict_t *ent, char *userinfo);
void ClientDisconnect (edict_t *ent);
void ClientBegin (edict_t *ent);
void ClientCommand (edict_t *ent);
void RunEntity (edict_t *ent);
void WriteGame (char *filename, qboolean autosave);
void ReadGame (char *filename);
void WriteLevel (char *filename);
void ReadLevel (char *filename);
void InitGame (void);
void G_RunFrame (void);


//===================================================================


void ShutdownGame (void)
{
	gi.dprintf ("==== ShutdownGame ====\n");

    /* SL - Log End of Game */
    sl_LogGameEnd( &gi, level.time );

    /* SL - Close Log File as Server is Shutting Down */
    sl_CloseLogFile();
	
	gi.FreeTags (TAG_LEVEL);
	gi.FreeTags (TAG_GAME);
}


//t-mek revert!
void startmatch(void)
{
	int i;
	edict_t	*who;
	edict_t *spot;
		
	spot = NULL;
	//reset teleport owners to null for both teams
	while ((spot = G_Find (spot, FOFS(classname), "info_player_team1")) != NULL)
	{
		spot->owner=NULL; //mark as unused spot
	}
	spot = NULL;
	while ((spot = G_Find (spot, FOFS(classname), "info_player_team2")) != NULL)
	{
		spot->owner=NULL; //mark as unused spot
	}
	
	for (i = 1; i <= maxclients->value; i++) {
		who = g_edicts + i;
		if (!who->client) continue;
		
		if(who->client->resp.ctf_team!=CTF_NOTEAM&&who->movetype==MOVETYPE_NOCLIP) {
			

			who->client->startmatchtime = level.time + (float)(i * 0.1);
			
			//who->svflags = 0;
			//who->client->resp.ctf_state = CTF_STATE_START;
			//PutClientInServer(who);
			//who->s.event = EV_PLAYER_TELEPORT;
			//who->client->ps.pmove.pm_flags = PMF_TIME_TELEPORT;
			//who->client->ps.pmove.pm_time = 14;	
		}

	}
}
//t-mek revert!


/*
=================
GetGameAPI

Returns a pointer to the structure with all entry points
and global variables
=================
*/
game_export_t *GetGameAPI (game_import_t *import)
{
	gi = *import;

	globals.apiversion = GAME_API_VERSION;
	globals.Init = InitGame;
	globals.Shutdown = ShutdownGame;
	globals.SpawnEntities = SpawnEntities;

	globals.WriteGame = WriteGame;
	globals.ReadGame = ReadGame;
	globals.WriteLevel = WriteLevel;
	globals.ReadLevel = ReadLevel;

	globals.ClientThink = ClientThink;
	globals.ClientConnect = ClientConnect;
	globals.ClientUserinfoChanged = ClientUserinfoChanged;
	globals.ClientDisconnect = ClientDisconnect;
	globals.ClientBegin = ClientBegin;
	globals.ClientCommand = ClientCommand;

	globals.RunFrame = G_RunFrame;

	globals.ServerCommand = ServerCommand;

	globals.edict_size = sizeof(edict_t);

	return &globals;
}

#ifndef GAME_HARD_LINKED
// this is only here so the functions in q_shared.c and q_shwin.c can link
void Sys_Error (char *error, ...)
{
	va_list		argptr;
	char		text[1024];

	va_start (argptr, error);
	vsprintf (text, error, argptr);
	va_end (argptr);

	gi.error (ERR_FATAL, "%s", text);
}

void Com_Printf (char *msg, ...)
{
	va_list		argptr;
	char		text[1024];

	va_start (argptr, msg);
	vsprintf (text, msg, argptr);
	va_end (argptr);

	gi.dprintf ("%s", text);
}

#endif

//======================================================================


/*
=================
ClientEndServerFrames
=================
*/
void ClientEndServerFrames (void)
{
	int		i;
	edict_t	*ent;

	// calc the player views now that all pushing
	// and damage has been added
	for (i=0 ; i<maxclients->value ; i++)
	{
		ent = g_edicts + 1 + i;
		if (!ent->inuse || !ent->client)
			continue;
		ClientEndServerFrame (ent);
	}

}

/*
=================
EndDMLevel

The timelimit or fraglimit has been exceeded
=================
*/
void EndDMLevel (void)
{
    edict_t     *ent = NULL;            /* Added '= NULL' - mdavies */

	ClearItems();
    // stay on same level flag
    if ((int)dmflags->value & DF_SAME_LEVEL)
    {
        ent = G_Spawn ();
        ent->classname = "target_changelevel";
        ent->map = level.mapname;
    }

    /* New code - START - mdavies */
    if( !ent )
    {
        ent = mdsoft_NextMap();
    }
    /* New code - END - mdavies */

    if( !ent )                          /* added line - mdavies */
    {                                   /* added line - mdavies */
        if (level.nextmap[0])           /* changed else if to if - mdavies */
        {   // go to a specific map
            ent = G_Spawn ();
            ent->classname = "target_changelevel";
            ent->map = level.nextmap;
        }
        else
        {   // search for a changeleve
            ent = G_Find (NULL, FOFS(classname), "target_changelevel");
            if (!ent)
            {   // the map designer didn't include a changelevel,
                // so create a fake ent that goes back to the same level
                ent = G_Spawn ();
                ent->classname = "target_changelevel";
                ent-> map = level.mapname;   // remove the space after the > to compile correctly
            }
        }
    }                                   /* Added line - mdavies */

    BeginIntermission (ent);
}


/*
=================
CheckDMRules
=================
*/
void CheckDMRules (void)
{
	int			i;
	gclient_t	*cl;

	if (level.intermissiontime)
		return;

	if (!deathmatch->value)
		return;

	if (timelimit->value)
	{
		if (level.time >= timelimit->value*60)
		{
			gi.bprintf (PRINT_HIGH, "Timelimit hit.\n");
			EndDMLevel ();
			return;
		}
	}


	//T-MEK moved code out of fraglimit loop below (was only being checked if fraglimit was set)
	//ZOID
		if (ctf->value) {
			if (CTFCheckRules()) {
				EndDMLevel ();
			}
		}
	//ZOID

	
	if (fraglimit->value)
	{
		for (i=0 ; i<maxclients->value ; i++)
		{
			cl = game.clients + i;
			if (!g_edicts[i+1].inuse)
				continue;

			if (cl->resp.score >= fraglimit->value)
			{
				gi.bprintf (PRINT_HIGH, "Fraglimit hit.\n");
				EndDMLevel ();
				return;
			}
		}
	}
}


/*
=============
ExitLevel
=============
*/
void ExitLevel (void)
{
	int		i;
	edict_t	*ent;
	char	command [256];

	Com_sprintf (command, sizeof(command), "gamemap \"%s\"\n", level.changemap);
	gi.AddCommandString (command);
	level.changemap = NULL;
	level.exitintermission = 0;
	level.intermissiontime = 0;
	ClientEndServerFrames ();

	// clear some things before going to next level
	for (i=0 ; i<maxclients->value ; i++)
	{
		ent = g_edicts + 1 + i;
		if (!ent->inuse)
			continue;
		if (ent->health > ent->client->pers.max_health)
			ent->health = ent->client->pers.max_health;
	}

//RAV
	hsran = false;
	show_hs = false;
	hs_show = true;
//

//ZOID
	CTFInit();
//ZOID

}

/*
================
G_RunFrame

Advances the world by 0.1 seconds
================
*/
void CheckMidnight();

void G_RunFrame (void)
{
	int		i;
	edict_t *e;
	edict_t	*ent;
	edict_t *best;	//t-mek

	best = NULL;	//t-mek

	level.framenum++;
	level.time = level.framenum*FRAMETIME;

	//T-MEK revert
	if (level.time==(matchdelay->value-11.5)){ //start countdown voice
		//gi.positioned_sound (nuke->s.origin, nuke, CHAN_AUTO, gi.soundindex ("world/10_0.wav"), .75, ATTN_NONE, 0);
		gi.sound (&g_edicts[0], CHAN_RELIABLE+CHAN_NO_PHS_ADD+CHAN_VOICE, gi.soundindex("world/10_0.wav"), 1, ATTN_NONE, 0);
	}

	if (level.time==(matchdelay->value-1)){ //start teleport sound - matchtime
		gi.sound (&g_edicts[0], CHAN_RELIABLE+CHAN_NO_PHS_ADD+CHAN_VOICE, gi.soundindex("misc/tele_up.wav"), 1, ATTN_NONE, 0);
	}


	if (level.time==(matchdelay->value-0.5)){ //allowplay  matchtime
		startmatch();
		gi.bprintf(PRINT_HIGH,"Match started.\n");
	}
	//t-mek revert
	
	// choose a client for monsters to target this frame
	AI_SetSightClient ();

	// exit intermissions

	if (level.intermissiontime && (level.intermissiontime < (level.time - 10)))
	{
		level.exitintermission = true;
	}

	if (level.exitintermission)
	{
		ExitLevel ();
		return;
	}

	//
	// treat each object in turn
	// even the world gets a chance to think
	//
	ent = &g_edicts[0];
	for (i=0 ; i<globals.num_edicts ; i++, ent++)
	{
		if (!ent->inuse)
			continue;

		level.current_entity = ent;

		VectorCopy (ent->s.origin, ent->s.old_origin);

		// if the ground entity moved, make sure we are still on it
		if ((ent->groundentity) && (ent->groundentity->linkcount != ent->groundentity_linkcount))
		{
			ent->groundentity = NULL;
			if ( !(ent->flags & (FL_SWIM|FL_FLY)) && (ent->svflags & SVF_MONSTER) )
			{
				M_CheckGround (ent);
			}
		}

		if (i > 0 && i <= maxclients->value)
		{

			//t-mek revert
			if((ent->client->resp.ctf_team!=CTF_NOTEAM)&&(ent->movetype==MOVETYPE_NOCLIP)&&(ent->client->startmatchtime==level.time)) {
		
				ent->svflags = 0;
				ent->client->resp.ctf_state = CTF_STATE_START;
				PutClientInServer(ent);
				ent->s.event = EV_PLAYER_TELEPORT;
				ent->client->ps.pmove.pm_flags = PMF_TIME_TELEPORT;
				ent->client->ps.pmove.pm_time = 14;	
			}
			//t-mek revert

			ClientBeginServerFrame (ent);

			//t-mek
			if (ent->client->resp.inplayerque != 0.0) //found a player wanting in
				if ( (!best) || ( ent->client->resp.inplayerque < best->client->resp.inplayerque) )
					best = ent;	
			//t-mek			
			continue;
		}

		G_RunEntity (ent);
	}

	//T-MEK: IF SERVER IS NOT FULL PUT LONGEST WAITING PLAYER ON A TEAM (only one per each server frame though)
	if (best)
		if (!MoreThanMaxPlayers()) 
			CTFJoinTeam( best, CTFSmallerTeam() );
	//T-MEK
	

	// see if it is time to end a deathmatch
	CheckDMRules ();

	//RAV
	if (level.intermissiontime)
	{ //set up highscores display

	//fix for highscores
		if(level.time == level.intermissiontime + 3.0)
		{
			highscore(); // first add players to the highscores list
			LoadHighScores(); // then load the new list.
		}
		if ((level.time >= level.intermissiontime + 4.0))
		{
			for(i=0;i<maxclients->value;i++) // add 1 to i until maxcl. value reached
			{
				e = g_edicts + 1 + i; //selects next player
				if(e->inuse && e->client)
				ShowHighScores (e);
				gi.unicast(e, false);
			}
		}

	}

	// build the playerstate_t structures for all players
	ClientEndServerFrames ();
}
