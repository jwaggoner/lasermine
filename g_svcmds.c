
#include "g_local.h"
#include "admin.h"

void LightsOff();
void LightsOn();

void Svcmd_Stuff (void)
{
	char	buff[256];
	char	*stuff;
	char	*s;
	edict_t	*ent;
	int id;

	id = -1;
	if (gi.argc() < 4)
	{
		gi.dprintf ("SV Stuff: Stuff any command to a client.\nUsage: sv stuff <userid> <command to stuff>\n");
		return;
	}

	if (!Find_Player_Edict_t(gi.argv(2)))
	{
		id = atoi(gi.argv(2));
		if (Q_stricmp(gi.argv(2), "0") != 0 && id == 0)
		{
			gi.dprintf("Player not found\n");
			return;
		}
		else
			ent = g_edicts + 1 + id;
	}
	else
		ent = Find_Player_Edict_t(gi.argv(2));

	if (!G_EntExists(ent))
	{
		gi.dprintf ("Player not found\n");
		return;
	}

	s = gi.args();
	stuff = s +  strlen(gi.argv(1)) + strlen(gi.argv(2)) + 2;
	sprintf (buff, "%s\n", stuff);
	gi.dprintf ("stuffing to client: %s\n", ent->client->pers.netname);
	gi.cprintf (ent, PRINT_HIGH, "You were stuffed the following command by the server admin: %s\n", buff);
	stuffcmd(ent, buff);
}

void Svcmd_Msg (void)
{
	int strnum = gi.argc();
	int line = 1;
	char msg [480], msg1[40], msg2[40], msg3[40], msg4[40], msg5[40];
	int i, j, k, l, m, n, o;
	edict_t *e;

	if (gi.argc() < 3)
	{
		gi.dprintf ("SV Msg: Prints a message to all clients from admin.\n");
		return;
	}

	j =  sprintf(msg1, "%s", gi.argv(2));

	for (i=3; i<strnum+1; i++)
	{
		switch (line)
		{
		case 1:
			{
				if (strlen(msg1) + strlen(gi.argv(i)) > 39)
				{
					k = sprintf(msg2, "%s", gi.argv(i));
					line = 2;
					break;
				}
				j += sprintf(msg1 + j, " %s", gi.argv(i));
				break;
			}
		case 2:
			{
				if (strlen(msg2) + strlen(gi.argv(i)) > 39)
				{
					l = sprintf(msg3, "%s", gi.argv(i));
					line = 3;
					break;
				}
				k += sprintf(msg2 + k, " %s", gi.argv(i));
				break;
			}
		case 3:
			{
				if (strlen(msg3) + strlen(gi.argv(i)) > 39)
				{
					m = sprintf(msg4, "%s", gi.argv(i));
					line = 4;
					break;
				}
				l += sprintf(msg3 + l, " %s", gi.argv(i));
				break;
			}
		case 4:
			{
				if (strlen(msg4) + strlen(gi.argv(i)) > 39)
				{
					n = sprintf(msg5, "%s", gi.argv(i));
					line = 5;
					break;
				}
				m += sprintf(msg4 + m, " %s", gi.argv(i));
				break;
			}
		case 5:
			{
				if (strlen(msg5) + strlen(gi.argv(i)) > 39)
				{
					gi.dprintf("Message exceeds max length, message will be truncated.\n");
					line = 6;
					break;
				}
				n += sprintf(msg5 + n, " %s", gi.argv(i));
				break;
			}
		default:
			{
				break;
			}
  		}
	}

	make_green (msg1);
	make_green (msg2);
	make_green (msg3);
	make_green (msg4);
	make_green (msg5);

	o = sprintf(msg, "%s\n", msg1);
	if (line > 1)
		o += sprintf (msg + o, "%s\n", msg2);
	if (line > 2)
		o += sprintf (msg + o, "%s\n", msg3);
	if (line > 3)
		o += sprintf (msg + o, "%s\n", msg4);
	if (line > 4)
		o += sprintf (msg + o, "%s\n", msg5);

	for (i=0 ; i<maxclients->value ; i++) // add 1 to i until maxcl. value reached
	{
		e = g_edicts + 1 + i; //selects next player
		if(e->inuse && e->client)
			gi.centerprintf(e, "Message from server administrator:\n\n%s", msg);
	}

}


/*
=================
ServerCommand

ServerCommand will be called when an "sv" command is issued.
The game can issue gi.argc() / gi.argv() commands to get the rest
of the parameters
=================
*/

void	ServerCommand (void)
{
	char	*cmd;
		
	cmd = gi.argv(1);
//JSW
	if	(Q_stricmp (cmd, "lightsoff") == 0)
		LightsOff();
	else if	(Q_stricmp (cmd, "lightson") == 0)
		LightsOn();
	else if (Q_stricmp (cmd, "msg") == 0)
		Svcmd_Msg ();
	else if (Q_stricmp (cmd, "stuff") == 0)
		Svcmd_Stuff ();
//JSW
	else
		gi.cprintf (NULL, PRINT_HIGH, "Unknown server command \"%s\"\n", cmd);
}


