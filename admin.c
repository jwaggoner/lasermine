#include "g_local.h"
#include "admin.h"

//
// Maj Bitch Helpers
//

//======================================================
// True if Ent is valid, has client, and edict_t inuse.
//======================================================
qboolean G_EntExists(edict_t *ent) {
  return ((ent) && (ent->client) && (ent->inuse));
}

//======================================================
// True if ent is not DEAD or DEAD or DEAD (and BURIED!)
//======================================================
qboolean G_ClientNotDead(edict_t *ent) {
qboolean buried=true;
qboolean b1=ent->client->ps.pmove.pm_type!=PM_DEAD;
qboolean b2=ent->deadflag != DEAD_DEAD;
qboolean b3=ent->health > 0;
  return ((b3 || b2 || b1) && buried);
}

//======================================================
// True if ent is not DEAD and not just did a Respawn.
//======================================================
qboolean G_ClientInGame(edict_t *ent)
{
  if (!G_EntExists(ent)) return false;
  if (!G_ClientNotDead(ent)) return false;
  return (ent->client->respawn_time + 5.0 < level.time);
}

//===================================================
// Simple centerprintf()
//===================================================
void gi_centerprintf(edict_t *ent, char *fmt, ...)
{
	char bigbuffer[0x10000];
	va_list argptr;
	int len;
	if (!ent || !ent->inuse || !ent->client)
		return;
	va_start(argptr,fmt);
	len = vsprintf(bigbuffer,fmt,argptr);
	va_end(argptr);
	gi.centerprintf(ent, bigbuffer);
}

//======================================================
// Use this function to centerprint to all players.
// e.g. Com_Centerprint_All("---<<< FIGHT ! >>>---\n");
//======================================================
void Centerprint_All(char *msg)
{
	int i;
	edict_t *ent;
	for(i=0;i<game.maxclients;i++)
	{
		ent=g_edicts+i+1;
		if (!G_EntExists(ent))
			continue;
		gi_centerprintf(ent, msg);
	}
}

char *make_green (char *text)
{
	unsigned char *p;
	p = (unsigned char *) text;

	while (*p)
	{
		if ((*p >= 0x1b && *p <= 0x7f) || (*p >= 0x0a && *p <= 0x11))
			*p += 0x80;
		p++;
	}
	return text;
}

//RAV
void GetTime()
 {
	int min,sec,hour;
	struct tm *ltime;  
	time_t gmtime;     
	time((unsigned long *)&gmtime);
	ltime=localtime((unsigned int *)&gmtime);

	min = ltime->tm_min;
	sec = ltime->tm_sec;
	hour = ltime->tm_hour;
	sprintf (am_pm, "AM");

	if (hour > 12)
	{
		hour -= 12;
		sprintf (am_pm, "PM");
	}

	if((min < 10)&&(sec < 10))
	{
	sprintf (sys_time,"%i:0%i:0%i", hour, min, sec);
	return;
	}

	if(min < 10)
	{
	sprintf (sys_time,"%i:0%i:%2i", hour, min, sec);
	return;
	}

	if(sec < 10)
	{
	sprintf (sys_time,"%i:%2i:0%i", hour, min, sec);
	return;
	}

	sprintf (sys_time,"%i:%2i:%2i", hour, min, sec);
  }

void GetDate()
{
	int day,month,year;
    struct tm *ltime;  
	time_t gmtime;     
	time((unsigned long *)&gmtime);
	ltime=localtime((unsigned int *)&gmtime);
  

	month = ltime->tm_mon+1;
	day = ltime->tm_mday;
	year = ltime->tm_year+1900;
	if(day < 10)
	sprintf (sys_date,"%i/0%1i/%4i", month, day, year);

	else
	sprintf (sys_date,"%i/%2i/%4i", month, day, year);

}

char *GameTime( void )
{
	int minutes;
	int seconds;
	static char timestring[6];
	
	seconds = level.time;
	seconds = seconds % 60;
	minutes = level.time / 60;
	sprintf(timestring, "%02d:%02d\n",minutes, seconds);
	return timestring;
}

void ShowLevelTime( edict_t *ent )
{
	gi.cprintf(ent,PRINT_HIGH,"Level Time:  %s\n",GameTime());
}

void ResetItems (void)
{
	int i;
	edict_t *ent;

	ent = &g_edicts[1];
	for (i=1; i < globals.num_edicts; i++, ent++)
	{
		if (!ent->inuse || ent->client || !ent->item)
			continue;

		if (ent->spawnflags & (DROPPED_ITEM | DROPPED_PLAYER_ITEM)) // Remove any dropped items
		{
			ent->nextthink = level.time;
			ent->think = G_FreeEdict;
		}
		else
			SetRespawn (ent, 0);
	}
}

void ClearItems (void)
{
	int i;
	edict_t *ent;

	ent = &g_edicts[1];
	for (i=1; i < globals.num_edicts; i++, ent++)
	{
		if (!ent->inuse || ent->client || !ent->item)
			continue;

		ent->nextthink = level.time;
		ent->think = G_FreeEdict;
	}
}

/* 
 * Have a spawn point disappear on us ("Phase Out").
 *
 * I use the G_Spawn_Sparks() and the gi.sound() routines to make the spawn
 * point disappear with a flourish, but you don't have to use any special
 * effects. If you aren't going to use any, ignore the first four lines and
 * just keep the "ent->" and "gi.linkentity" lines.
 */
void SP_misc_spawn_point_phase_out (edict_t *ent)
{
	vec3_t movedir = {0, 0, 1};

	//G_Spawn_Sparks(TE_SHIELD_SPARKS, ent->s.origin, movedir, ent->s.origin);
	//G_Spawn_Sparks(TE_BULLET_SPARKS, ent->s.origin, movedir, ent->s.origin);
	G_Spawn_Sparks(TE_SHIELD_SPARKS, ent->s.origin, movedir, ent->s.origin);

//	gi.sound(ent, CHAN_BODY, gi.soundindex ("tank/pain.wav"), 1, ATTN_IDLE, 0);

	ent->solid = SOLID_NOT;
	ent->think = NULL;
	ent->svflags |= SVF_NOCLIENT;
	gi.linkentity (ent);
}

/* 
 * Spawn points start out invisible. When used they appear for 5 seconds.
 */
void SP_misc_phase_in_spawn_point (edict_t *ent)
{
	if (!ent)
		return;

	if (ent->solid != SOLID_NOT)
	{
		/*
		 * We've been called a second time on the same spawn point
		 * before it's had a chance to phase out. Let's make sure
		 * that it sticks around a little longer than the first spawn
		 * in time so that the 2nd spawn person isn't startled by an
		 * early disappearance.
		 */
		if (ent->think == SP_misc_spawn_point_phase_out)
			ent->nextthink = 5 + level.time;
		return;
	}

	/* make sure we don't exist, so we don't interfere with the KillBox */
	gi.unlinkentity (ent);
	KillBox (ent);

	ent->solid = SOLID_BBOX;
	ent->svflags &= ~SVF_NOCLIENT;
	ent->nextthink = 5 + level.time;
	ent->think = SP_misc_spawn_point_phase_out;
	gi.linkentity (ent);
}


//======================================================
/*
 Spawns sparks of (type) from (start) in direction of (movdir) and
 Broadcasts to all in Potentially Visible Set from vector (origin)
     TE_BLASTER - Spawns a blaster sparks
     TE_BLOOD - Spawns a spurt of red blood
     TE_BULLET_SPARKS - Same as TE_SPARKS, with a bullet puff and richochet sound
     TE_GREENBLOOD - NOT IMPLEMENTED - Spawns a spurt of green blood
     TE_GUNSHOT - Spawns a grey splash of particles, with a bullet puff
     TE_SCREEN_SPARKS - Spawns a large green/white splash of sparks
     TE_SHIELD_SPARKS - Spawns a large blue/violet splash of sparks
     TE_SHOTGUN - Spawns a small grey splash of spark particles, with a bullet puff
     TE_SPARKS - Spawns a red/gold splash of spark particles
     TE_BLASTER2 - New to v3.20
     TE_MOREBLOOD - New to v3.20
     TE_CHAINFIST_SMOKE - New to v3.20
     TE_TUNNEL_SPARKS - New to v3.20
     TE_ELECTRIC_SPARKS - New to v3.20
     TE_HEATBEAM_SPARKS - New to v3.20
     TE_BLUEHYPERBLASTER - New to v3.20
*/
//======================================================
void G_Spawn_Sparks(int type, vec3_t start, vec3_t movdir, vec3_t origin )
{
	gi.WriteByte(svc_temp_entity);
	gi.WriteByte(type);
	gi.WritePosition(start);
	gi.WriteDir(movdir);
	gi.multicast(origin, MULTICAST_PVS);
}
//======================================================

edict_t *Find_Player_Edict_t (char *s)
{
	int i;
	edict_t *player;
	for(i=1;i<=maxclients->value;i++)
	{
		if ((player=&g_edicts[i]) && player->inuse && (Q_stricmp(s, player->client->pers.netname) == 0))
			return player;
	}
	return NULL;
}
