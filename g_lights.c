//
// g_lights.c
//
//
// 05/05/2001 Josh Waggoner (greider)

#include "g_local.h"
#include "g_lights.h"

void LightsOff(void)
{
	int i; 
	edict_t	*e;

	if (!lights->value == 0)
	{
		// 0 normal
		gi.configstring(CS_LIGHTS+0, "b");        
		// 1 FLICKER (first variety)
		gi.configstring(CS_LIGHTS+1, "a");        
		// 2 SLOW STRONG PULSE
		gi.configstring(CS_LIGHTS+2, "b");        
		// 3 CANDLE (first variety)
		gi.configstring(CS_LIGHTS+3, "a");        
		// 4 FAST STROBE
		gi.configstring(CS_LIGHTS+4, "b");        
		// 5 GENTLE PULSE 1
		gi.configstring(CS_LIGHTS+5,"a");        
		// 6 FLICKER (second variety)
		gi.configstring(CS_LIGHTS+6, "b");        
		// 7 CANDLE (second variety)
		gi.configstring(CS_LIGHTS+7, "a");        
		// 8 CANDLE (third variety)
		gi.configstring(CS_LIGHTS+8, "b");        
		// 9 SLOW STROBE (fourth variety)
		gi.configstring(CS_LIGHTS+9, "a");        
		// 10 FLUORESCENT FLICKER
		gi.configstring(CS_LIGHTS+10, "b");
		// 11 SLOW PULSE NOT FADE TO BLACK
		gi.configstring(CS_LIGHTS+11, "a");
		gi.cvar_forceset("lights", "0");
		
		for (i=0 ; i<maxclients->value ; i++) // add 1 to i until maxcl. value reached
		{
			e = g_edicts + 1 + i; //selects next player
			gi.sound (e, CHAN_AUTO, gi.soundindex ("world/fuseout.wav"), 1, ATTN_NORM, 0);
		}
	}
}

void LightsOn(void)
{
	int i; 
	edict_t *e;

	if (!lights->value == 1)
	{
		// 0 normal
		gi.configstring(CS_LIGHTS+0, "m");        
		// 1 FLICKER (first variety)
		gi.configstring(CS_LIGHTS+1, "mmnmmommommnonmmonqnmmo");
		// 2 SLOW STRONG PULSE
		gi.configstring(CS_LIGHTS+2, "abcdefghijklmnopqrstuvwxyzyxwvutsrqponmlkjihgfedcba");
		// 3 CANDLE (first variety)
		gi.configstring(CS_LIGHTS+3, "mmmmmaaaaammmmmaaaaaabcdefgabcdefg");
		// 4 FAST STROBE
		gi.configstring(CS_LIGHTS+4, "mamamamamama");        
		// 5 GENTLE PULSE 1
		gi.configstring(CS_LIGHTS+5,"jklmnopqrstuvwxyzyxwvutsrqponmlkj");
		// 6 FLICKER (second variety)
		gi.configstring(CS_LIGHTS+6, "nmonqnmomnmomomno");
		// 7 CANDLE (second variety)
		gi.configstring(CS_LIGHTS+7, "mmmaaaabcdefgmmmmaaaammmaamm");
		// 8 CANDLE (third variety)
		gi.configstring(CS_LIGHTS+8, "mmmaaammmaaammmabcdefaaaammmmabcdefmmmaaaa");
		// 9 SLOW STROBE (fourth variety)
		gi.configstring(CS_LIGHTS+9, "aaaaaaaazzzzzzzz");
		// 10 FLUORESCENT FLICKER
		gi.configstring(CS_LIGHTS+10, "mmamammmmammamamaaamammma");
		// 11 SLOW PULSE NOT FADE TO BLACK
		gi.configstring(CS_LIGHTS+11, "abcdefghijklmnopqrrqponmlkjihgfedcba");
		//JR 3/26/98
		gi.cvar_forceset("lights", "1");
			
		for (i=0 ; i<maxclients->value ; i++) // add 1 to i until maxcl. value reached
		{
			e = g_edicts + 1 + i; //selects next player
			gi.sound (e, CHAN_AUTO, gi.soundindex ("world/lite_on3.wav"), 1, ATTN_NORM, 0);
		}
	}
}

//Thanks to RAVEN for this snippet
void CheckMidnight()
{
	int i;
	int on=0;
	int off=0;
	edict_t *e;
			
	for (i=0 ; i < maxclients->value ; ++i)
	{
		e = g_edicts + 1 + i;
		
		//do check for client
		if (!e || !e->client)
			continue;
		
		//see if he voted or not
		if(!e->client->midnight_vote)//dont care so skip him 
			continue;
	
		if(e->client->midnight_vote == 1)//on
			on++;
	
		if(e->client->midnight_vote == 2)//off
			off++;
	}
	
	if(on >= off)
	{
		if (!allow_midnight->value)
			return;
		LightsOn();
		return;
	}
	
	if(off > on)
	{
		if (!allow_midnight->value)
			return;
		LightsOff();
		return;
	}
}
