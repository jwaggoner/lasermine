/******************************************************************************
**
**	Standard Logging version defines
**
**
**	$Id$
**
**  Copyright (c) 1998-1999 Mark Davies.
**  Distributed under the "Artistic License".
**  Please read the file artistic.txt for complete licensing and
**  redistribution information.
**
******************************************************************************/

#define SL_MAJOR_VERSION    "1"
#define SL_MINOR_VERSION    "0"

#define SL_CVS_COUNTER      "0"

#define SL_VERSION_STRING   SL_MAJOR_VERSION "." SL_MINOR_VERSION " (" SL_CVS_COUNTER ")"

/* end of file */
