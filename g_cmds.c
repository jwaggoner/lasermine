#include "g_local.h"
#include "m_player.h"
#include "admin.h"
#include "micronuke.h"
#include "lasrmine.h"

edict_t *head;  //t-mek revert

char *CTFTeamName(int team);

//==============================================================
char *ClientTeam(edict_t *ent)\
{
	char *p;
	static char value[512];

	value[0]=0;

	// Make sure ent exists!
	if (!G_EntExists(ent))
		return value;

	strcpy(value, Info_ValueForKey (ent->client->pers.userinfo, "skin"));
	p = strchr(value, '/');
	if (!p)
		return value;

	if ((int)(dmflags->value) & DF_MODELTEAMS)
	{
		*p = 0;
		return value;
	}

	// if ((int)(dmflags->value) & DF_SKINTEAMS)
	return ++p;
}

qboolean OnSameTeam (edict_t *ent1, edict_t *ent2)
{
	char	ent1Team [512];
	char	ent2Team [512];

	if (!((int)(dmflags->value) & (DF_MODELTEAMS | DF_SKINTEAMS)))
		return false;

	strcpy (ent1Team, ClientTeam (ent1));
	strcpy (ent2Team, ClientTeam (ent2));

	if (strcmp(ent1Team, ent2Team) == 0)
		return true;
	return false;
}


void SelectNextItem (edict_t *ent, int itflags)
{
	gclient_t	*cl;
	int			i, index;
	gitem_t		*it;

	cl = ent->client;

	if (cl->flood_mute == true)
		return;

//ZOID
	if (cl->menu) {
		PMenu_Next(ent);
		return;
	} else if (cl->chase_target) {
		ChaseNext(ent);
		return;
	}
//ZOID

	// scan  for the next valid one
	for (i=1 ; i<=MAX_ITEMS ; i++)
	{
		index = (cl->pers.selected_item + i)%MAX_ITEMS;
		if (!cl->pers.inventory[index])
			continue;
		it = &itemlist[index];
		if (it == FindItem("Spam Cannon"))
			it = &itemlist[index+1];
		if (!it->use)
			continue;
		if (!(it->flags & itflags))
			continue;
		cl->pers.selected_item = index;
		return;
	}

	cl->pers.selected_item = -1;
}

void SelectPrevItem (edict_t *ent, int itflags)
{
	gclient_t	*cl;
	int			i, index;
	gitem_t		*it;

	cl = ent->client;

	if (cl->flood_mute == true)
		return;

//ZOID
	if (cl->menu) {
		PMenu_Prev(ent);
		return;
	} else if (cl->chase_target) {
		ChasePrev(ent);
		return;
	}
//ZOID

	// scan  for the next valid one
	for (i=1 ; i<=MAX_ITEMS ; i++)
	{
		index = (cl->pers.selected_item + MAX_ITEMS - i)%MAX_ITEMS;
		if (!cl->pers.inventory[index])
			continue;
		it = &itemlist[index];
		if (it == FindItem("Spam Cannon"))
			it = &itemlist[index+1];
		if (!it->use)
			continue;
		if (!(it->flags & itflags))
			continue;
		cl->pers.selected_item = index;
		return;
	}

	cl->pers.selected_item = -1;
}

void ValidateSelectedItem (edict_t *ent)
{
	gclient_t	*cl;

	cl = ent->client;

	if (cl->pers.inventory[cl->pers.selected_item])
		return;		// valid

	SelectNextItem (ent, -1);
}

//=================================================================================

/*
==================
Cmd_Give_f

Give items to a client
==================
*/
void Cmd_Give_f (edict_t *ent)
{
	char		*name;
	gitem_t		*it;
	int			index;
	int			i;
	qboolean	give_all;
	edict_t		*it_ent;

	if (deathmatch->value && !sv_cheats->value)
	{
		gi.cprintf (ent, PRINT_HIGH, "You must run the server with '+set cheats 1' to enable this command.\n");
		return;
	}

	name = gi.args();

	if (Q_stricmp(name, "all") == 0)
		give_all = true;
	else
		give_all = false;

	if (give_all || Q_stricmp(gi.argv(1), "health") == 0)
	{
		if (gi.argc() == 3)
			ent->health = atoi(gi.argv(2));
		else
			ent->health = ent->max_health;
		if (!give_all)
			return;
	}

	if (give_all || Q_stricmp(name, "weapons") == 0)
	{
		for (i=0 ; i<game.num_items ; i++)
		{
			it = itemlist + i;
			if (!it->pickup)
				continue;
			if (!(it->flags & IT_WEAPON))
				continue;
			ent->client->pers.inventory[i] += 1;
		}
		if (!give_all)
			return;
	}

	if (give_all || Q_stricmp(name, "ammo") == 0)
	{
		for (i=0 ; i<game.num_items ; i++)
		{
			it = itemlist + i;
			if (!it->pickup)
				continue;
			if (!(it->flags & IT_AMMO))
				continue;
			Add_Ammo (ent, it, 1000);
		}
		if (!give_all)
			return;
	}

	if (give_all || Q_stricmp(name, "armor") == 0)
	{
		gitem_armor_t	*info;

		it = FindItem("Jacket Armor");
		ent->client->pers.inventory[ITEM_INDEX(it)] = 0;

		it = FindItem("Combat Armor");
		ent->client->pers.inventory[ITEM_INDEX(it)] = 0;

		it = FindItem("Body Armor");
		info = (gitem_armor_t *)it->info;
		ent->client->pers.inventory[ITEM_INDEX(it)] = info->max_count;

		if (!give_all)
			return;
	}

	if (give_all || Q_stricmp(name, "Power Shield") == 0)
	{
		it = FindItem("Power Shield");
		it_ent = G_Spawn();
		it_ent->classname = it->classname;
		SpawnItem (it_ent, it);
		Touch_Item (it_ent, ent, NULL, NULL);
		if (it_ent->inuse)
			G_FreeEdict(it_ent);

		if (!give_all)
			return;
	}

	if (give_all)
	{
		for (i=0 ; i<game.num_items ; i++)
		{
			it = itemlist + i;
			if (!it->pickup)
				continue;
			if (it->flags & (IT_ARMOR|IT_WEAPON|IT_AMMO))
				continue;
			ent->client->pers.inventory[i] = 1;
		}
		return;
	}

	it = FindItem (name);
	if (!it)
	{
		name = gi.argv(1);
		it = FindItem (name);
		if (!it)
		{
			gi.dprintf ("unknown item\n");
			return;
		}
	}

	if (!it->pickup)
	{
		gi.dprintf ("non-pickup item\n");
		return;
	}

	index = ITEM_INDEX(it);

	if (it->flags & IT_AMMO)
	{
		if (gi.argc() == 3)
			ent->client->pers.inventory[index] = atoi(gi.argv(2));
		else
			ent->client->pers.inventory[index] += it->quantity;
	}
	else
	{
		it_ent = G_Spawn();
		it_ent->classname = it->classname;
		SpawnItem (it_ent, it);
		Touch_Item (it_ent, ent, NULL, NULL);
		if (it_ent->inuse)
			G_FreeEdict(it_ent);
	}
}


/*
==================
Cmd_God_f

Sets client to godmode

argv(0) god
==================
*/
void Cmd_God_f (edict_t *ent)
{
	char	*msg;

	if (deathmatch->value && !sv_cheats->value)
	{
		gi.cprintf (ent, PRINT_HIGH, "You must run the server with '+set cheats 1' to enable this command.\n");
		return;
	}

	ent->flags ^= FL_GODMODE;
	if (!(ent->flags & FL_GODMODE) )
		msg = "godmode OFF\n";
	else
		msg = "godmode ON\n";

	gi.cprintf (ent, PRINT_HIGH, msg);
}


/*
==================
Cmd_Notarget_f

Sets client to notarget

argv(0) notarget
==================
*/
void Cmd_Notarget_f (edict_t *ent)
{
	char	*msg;

	if (deathmatch->value && !sv_cheats->value)
	{
		gi.cprintf (ent, PRINT_HIGH, "You must run the server with '+set cheats 1' to enable this command.\n");
		return;
	}

	ent->flags ^= FL_NOTARGET;
	if (!(ent->flags & FL_NOTARGET) )
		msg = "notarget OFF\n";
	else
		msg = "notarget ON\n";

	gi.cprintf (ent, PRINT_HIGH, msg);
}


/*
==================
Cmd_Noclip_f

argv(0) noclip
==================
*/
void Cmd_Noclip_f (edict_t *ent)
{
	char	*msg;

	if (deathmatch->value && !sv_cheats->value)
	{
		gi.cprintf (ent, PRINT_HIGH, "You must run the server with '+set cheats 1' to enable this command.\n");
		return;
	}

	if (ent->movetype == MOVETYPE_NOCLIP)
	{
		ent->movetype = MOVETYPE_WALK;
		msg = "noclip OFF\n";
	}
	else
	{
		ent->movetype = MOVETYPE_NOCLIP;
		msg = "noclip ON\n";
	}

	gi.cprintf (ent, PRINT_HIGH, msg);
}


/*
==================
Cmd_Use_f

Use an inventory item
==================
*/
void Cmd_Use_f (edict_t *ent)
{
	int			index;
	gitem_t		*it;
	gitem_t		*spamcannon;
	char		*s;

	// Not available to dead or respawning players!
	if (!G_EntExists(ent))
		return;

	if (ent->client->flood_mute == true && use_spam_cannon->value)
		return;

	s = gi.args();
	it = FindItem (s);
	spamcannon = FindItem ("Spam Cannon");

	
	if (it == spamcannon)
	{
		gi.cprintf (ent, PRINT_HIGH, "HaHa The Spam Cannon is for special use only!!");
		return;
	}

	if (!it)
	{
		gi.cprintf (ent, PRINT_HIGH, "unknown item: %s\n", s);
		return;
	}
	if (!it->use)
	{
		gi.cprintf (ent, PRINT_HIGH, "Item is not usable.\n");
		return;
	}
	index = ITEM_INDEX(it);
	if (!ent->client->pers.inventory[index])
	{
		gi.cprintf (ent, PRINT_HIGH, "Out of item: %s\n", s);
		return;
	}

	it->use (ent, it);
}


/*
==================
Cmd_Drop_f

Drop an inventory item
==================
*/
void Cmd_Drop_f (edict_t *ent)
{
	int			index;
	gitem_t		*it;
	char		*s;

	// Not available to dead or respawning players!
	if (!G_ClientInGame(ent))
		return;

	if (ent->client->resp.ctf_team == CTF_NOTEAM)
		return;

//ZOID--special case for tech powerups
	if (Q_stricmp(gi.args(), "tech") == 0 && (it = CTFWhat_Tech(ent)) != NULL) {
		it->drop (ent, it);
		return;
	}
//ZOID

	s = gi.args();
	it = FindItem (s);
	if (!it)
	{
		gi.cprintf (ent, PRINT_HIGH, "unknown item: %s\n", s);
		return;
	}
	if (!it->drop)
	{
		gi.cprintf (ent, PRINT_HIGH, "Item is not droppable.\n");
		return;
	}
	index = ITEM_INDEX(it);
	if (!ent->client->pers.inventory[index])
	{
		gi.cprintf (ent, PRINT_HIGH, "Out of item: %s\n", s);
		return;
	}

	it->drop (ent, it);
}

/*
=================
Cmd_FireFlare_f
=================
*/
static void Flare_Touch (edict_t *ent, edict_t *other, cplane_t *plane, csurface_t *surf);
void flare_think2(edict_t *ent);
void fire_flare (edict_t *self, vec3_t start, vec3_t aimdir, int speed);

void Cmd_FireFlare_f (edict_t *ent)
{
	// Not available to dead or respawning players!
	if (!G_ClientInGame(ent))
		return;

	if (ent->client->resp.ctf_team == CTF_NOTEAM)
	{
		gi.centerprintf(ent, "Spectators are not allowed to fire flares.");
		return;
	}

	if (ent->waterlevel > 1)
	{
#ifdef _DEBUG
		gi.dprintf ("Flare fired from water\n");
#endif
		return;
	}

	else if (lights->value == 0)
	{
		vec3_t	offset;
		vec3_t	forward, right;
		vec3_t	start;
	
		VectorSet(offset, 8, 8, ent->viewheight-8);
		AngleVectors (ent->client->v_angle, forward, right, NULL);
		P_ProjectSource (ent->client, ent->s.origin, offset, forward, right, start);
	
		fire_flare (ent, start, forward, 1500);
	}

	else if (lights->value == 1)
		gi.centerprintf(ent, "Flares not allowed with lights on.\n");
}


/*
=================
Cmd_Inven_f
=================
*/
void Cmd_Inven_f (edict_t *ent)
{
	int			i;
	gclient_t	*cl;

	cl = ent->client;

	if (cl->flood_mute == true && use_spam_cannon->value)
		return;

	cl->showscores = false;
	cl->showhelp = false;

//ZOID
	if (ent->client->menu) {
		PMenu_Close(ent);
		ent->client->update_chase = true;
		return;
	}
//ZOID

	if (cl->showinventory)
	{
		cl->showinventory = false;
		return;
	}

//ZOID
	if (ctf->value && cl->resp.ctf_team == CTF_NOTEAM) {
		CTFOpenJoinMenu(ent);
		return;
	}
//ZOID

	cl->showinventory = true;

	gi.WriteByte (svc_inventory);
	for (i=0 ; i<MAX_ITEMS ; i++)
	{
		gi.WriteShort (cl->pers.inventory[i]);
	}
	gi.unicast (ent, true);
}

/*
=================
Cmd_InvUse_f
=================
*/
void Cmd_InvUse_f (edict_t *ent)
{
	gitem_t		*it;

	// Not available to dead or respawning players!
	if (!G_ClientInGame(ent))
		return;

	if (ent->client->flood_mute == true)
		return;

//ZOID
	if (ent->client->menu) {
		PMenu_Select(ent);
		return;
	}
//ZOID

	ValidateSelectedItem (ent);

	if (ent->client->pers.selected_item == -1)
	{
		gi.cprintf (ent, PRINT_HIGH, "No item to use.\n");
		return;
	}

	it = &itemlist[ent->client->pers.selected_item];
	if (!it->use)
	{
		gi.cprintf (ent, PRINT_HIGH, "Item is not usable.\n");
		return;
	}
	it->use (ent, it);
}

//ZOID

/*
=================
Cmd_OpenJoinMenu_f
=================
Josh Waggoner (greider)
*/
void Cmd_OpenJoinMenu_f (edict_t *ent)
{
	gclient_t	*cl;

	cl = ent->client;

	cl->showscores = false;
	cl->showhelp = false;

	if (ent->client->menu) {
		PMenu_Close(ent);
		ent->client->update_chase = true;
		return;
	}

	CTFOpenJoinMenu(ent);

}

/*
=================
Cmd_LastWeap_f
=================
*/
void Cmd_LastWeap_f (edict_t *ent)
{
	gclient_t	*cl;

	cl = ent->client;

	// Not available to dead or respawning players!
	if (!G_ClientInGame(ent))
		return;

	if (cl->flood_mute == true)
		return;
	if (cl->pers.lastweapon == FindItem("Spam Cannon"))
		return;
	if (!cl->pers.weapon || !cl->pers.lastweapon)
		return;

	cl->pers.lastweapon->use (ent, cl->pers.lastweapon);
}
//ZOID

/*
=================
Cmd_WeapPrev_f
=================
*/
void Cmd_WeapPrev_f (edict_t *ent)
{
	gclient_t	*cl;
	int			i, index;
	gitem_t		*it;
	int			selected_weapon;

	cl = ent->client;

	// Not available to dead or respawning players!
	if (!G_ClientInGame(ent))
		return;
	if (cl->flood_mute == true)
		return;
	if (!cl->pers.weapon)
		return;

	selected_weapon = ITEM_INDEX(cl->pers.weapon);

	// scan  for the next valid one
	for (i=1 ; i<=MAX_ITEMS ; i++)
	{
		index = (selected_weapon + i)%MAX_ITEMS;
		if (!cl->pers.inventory[index])
			continue;
		it = &itemlist[index];
		if (it == FindItem("Spam Cannon"))
			it = &itemlist[index+1];
		if (!it->use)
			continue;
		if (! (it->flags & IT_WEAPON) )
			continue;
		it->use (ent, it);
		if (cl->pers.weapon == it)
			return;	// successful
	}
}

/*
=================
Cmd_WeapNext_f
=================
*/
void Cmd_WeapNext_f (edict_t *ent)
{
	gclient_t	*cl;
	int			i, index;
	gitem_t		*it;
	int			selected_weapon;

	cl = ent->client;

	// Not available to dead or respawning players!
	if (!G_ClientInGame(ent))
		return;

	if (cl->flood_mute == true)
		return;

	if (!cl->pers.weapon)
		return;

	selected_weapon = ITEM_INDEX(cl->pers.weapon);

	// scan  for the next valid one
	for (i=1 ; i<=MAX_ITEMS ; i++)
	{
		index = (selected_weapon + MAX_ITEMS - i)%MAX_ITEMS;
		if (!cl->pers.inventory[index])
			continue;
		it = &itemlist[index];
		if (it == FindItem("Spam Cannon"))
			it = &itemlist[index+1];
		if (!it->use)
			continue;
		if (! (it->flags & IT_WEAPON) )
			continue;
		it->use (ent, it);
		if (cl->pers.weapon == it)
			return;	// successful
	}
}

/*
=================
Cmd_WeapLast_f
=================
*/
void Cmd_WeapLast_f (edict_t *ent)
{
	gclient_t	*cl;
	int			index;
	gitem_t		*it;

	cl = ent->client;

	// Not available to dead or respawning players!
	if (!G_ClientInGame(ent))
		return;

	if (!cl->pers.weapon || !cl->pers.lastweapon)
		return;

	index = ITEM_INDEX(cl->pers.lastweapon);
	if (!cl->pers.inventory[index])
		return;
	it = &itemlist[index];
	if (!it->use)
		return;
	if (! (it->flags & IT_WEAPON) )
		return;
	it->use (ent, it);
}

/*
=================
Cmd_InvDrop_f
=================
*/
void Cmd_InvDrop_f (edict_t *ent)
{
	gitem_t		*it;

	// Not available to dead or respawning players!
	if (!G_ClientInGame(ent))
		return;
	
	ValidateSelectedItem (ent);

	if (ent->client->pers.selected_item == -1)
	{
		gi.cprintf (ent, PRINT_HIGH, "No item to drop.\n");
		return;
	}

	it = &itemlist[ent->client->pers.selected_item];
	if (!it->drop)
	{
		gi.cprintf (ent, PRINT_HIGH, "Item is not droppable.\n");
		return;
	}
	it->drop (ent, it);
}

/*
=================
Cmd_Kill_f
=================
*/
void Cmd_Kill_f (edict_t *ent)
{
	// Not available to dead or respawning players!
	if (!G_ClientInGame(ent))
		return;
	
//ZOID
	if (ent->solid == SOLID_NOT)
		return;
//ZOID

	if((level.time - ent->client->respawn_time) < 5)
		return;
	ent->flags &= ~FL_GODMODE;
	ent->health = 0;
	meansOfDeath = MOD_SUICIDE;
	player_die (ent, ent, ent, 100000, vec3_origin);
	// don't even bother waiting for death frames
	ent->deadflag = DEAD_DEAD;
	respawn (ent);
}

/*
=================
Cmd_PutAway_f
=================
*/
void Cmd_PutAway_f (edict_t *ent)
{
	ent->client->showscores = false;
	ent->client->showhelp = false;
	ent->client->showinventory = false;
//ZOID
	if (ent->client->menu)
		PMenu_Close(ent);
	ent->client->update_chase = true;
//ZOID
}


int PlayerSort (void const *a, void const *b)
{
	int		anum, bnum;

	anum = *(int *)a;
	bnum = *(int *)b;

	anum = game.clients[anum].ps.stats[STAT_FRAGS];
	bnum = game.clients[bnum].ps.stats[STAT_FRAGS];

	if (anum < bnum)
		return -1;
	if (anum > bnum)
		return 1;
	return 0;
}

/*
=================
Cmd_Players_f
=================
*/
void Cmd_Players_f (edict_t *ent)
{
	int		i;
	int		count;
	char	small[64];
	char	large[1280];
	char	*teamname;
	int		index[256];
	edict_t *who;  //t-mek

	count = 0;
	for (i = 1 ; i <= maxclients->value ; i++){

		who = g_edicts + i;
		if (who->inuse)  //t-mek fixed this function (was counting disconnected playes)
			{
				index[count] = i-1;
				count++;
			}

	}
	// sort by frags
	qsort (index, count, sizeof(index[0]), PlayerSort);

	// print information
	large[0] = 0;

	for (i = 0 ; i < count ; i++)
	{
		teamname = CTFTeamName(game.clients[index[i]].resp.ctf_team);
		Com_sprintf (small, sizeof(small), "%2i - %4s %3i %s \n",
			i,
			teamname,
			game.clients[index[i]].ps.stats[STAT_FRAGS],
			game.clients[index[i]].pers.netname);
		if (strlen (small) + strlen(large) > sizeof(large) - 100 )
		{	// can't print all of them in one packet
			strcat (large, "...\n");
			break;
		}
		strcat (large, small);
	}

	gi.cprintf (ent, PRINT_HIGH, "%s\n%i players\n", large, count);
}

/*
=================
Cmd_Wave_f
=================
*/
void Cmd_Wave_f (edict_t *ent)
{
	int		i;

	// Not available to dead or respawning players!
	if (!G_ClientInGame(ent))
		return;

	if (ent->movetype == MOVETYPE_NOCLIP)  // t-mek fix
		return;  // spectators cannot wave!!! t-mek

	i = atoi (gi.argv(1));

	// can't wave when ducked
	if (ent->client->ps.pmove.pm_flags & PMF_DUCKED)
		return;

	if (ent->client->anim_priority > ANIM_WAVE)
		return;

	ent->client->anim_priority = ANIM_WAVE;

	switch (i)
	{
	case 0:
		gi.cprintf (ent, PRINT_HIGH, "flipoff\n");
		ent->s.frame = FRAME_flip01-1;
		ent->client->anim_end = FRAME_flip12;
		break;
	case 1:
		gi.cprintf (ent, PRINT_HIGH, "salute\n");
		ent->s.frame = FRAME_salute01-1;
		ent->client->anim_end = FRAME_salute11;
		break;
	case 2:
		gi.cprintf (ent, PRINT_HIGH, "taunt\n");
		ent->s.frame = FRAME_taunt01-1;
		ent->client->anim_end = FRAME_taunt17;
		break;
	case 3:
		gi.cprintf (ent, PRINT_HIGH, "wave\n");
		ent->s.frame = FRAME_wave01-1;
		ent->client->anim_end = FRAME_wave11;
		break;
	case 4:
	default:
		gi.cprintf (ent, PRINT_HIGH, "point\n");
		ent->s.frame = FRAME_point01-1;
		ent->client->anim_end = FRAME_point12;
		break;
	}
}

/*
==================
Cmd_Say_f
==================
*/
void Cmd_Say_f (edict_t *ent, qboolean team, qboolean arg0)
{
	int		j;
	edict_t	*other;
	char	*p;
	char	text[2048];
	int		mutetime;

	if (gi.argc () < 2 && !arg0)
		return;

//t-mek flood protection
	
	
	//test if flooding
	if (ent->client->flood_msgs >= flood_msgs->value)
	{
		mutetime=ent->client->flood_reset-level.time;
		if(ent->client->flood_mute==true)
			gi.cprintf(ent,PRINT_HIGH,"You can't talk for %d more seconds\n",mutetime);
		else
		{
			// set mute time
			ent->client->flood_reset = level.time + flood_waitdelay->value;
			gi.centerprintf(ent,"Flood protection:  You can't talk\nfor %i seconds.\n",(int)flood_waitdelay->value);
			ent->client->flood_mute = true;
			if (use_spam_cannon->value)
			{
				if (ent->client->resp.ctf_team != CTF_NOTEAM)
				{
					ent->client->pers.lastweapon = ent->client->pers.weapon;
					ent->client->newweapon = FindItem ("Spam Cannon");
					ChangeWeapon (ent);
					stuffcmd(ent, "+attack");
				}
			}

			gi.bprintf(PRINT_HIGH,"%s has been muted for %i seconds.\n",ent->client->pers.netname,(int)flood_waitdelay->value);
		}
		return;
	}

	//increment message count and set new reset time
	ent->client->flood_msgs++;
	ent->client->flood_reset = level.time + flood_persecond->value;	

//t-mek
	
	if (!((int)(dmflags->value) & (DF_MODELTEAMS | DF_SKINTEAMS)))
		team = false;

	if (team)
		Com_sprintf (text, sizeof(text), "(%s): ", ent->client->pers.netname);
	else
		Com_sprintf (text, sizeof(text), "%s: ", ent->client->pers.netname);

	if (arg0)
	{
		strcat (text, gi.argv(0));
		strcat (text, " ");
		strcat (text, gi.args());
	}
	else
	{
		p = gi.args();

		if (*p == '"')
		{
			p++;
			p[strlen(p)-1] = 0;
		}
		strcat(text, p);
	}

	// don't let text be too long for malicious reasons
	if (strlen(text) > 150)
		text[150] = 0;

	strcat(text, "\n");

	if (dedicated->value)
		gi.cprintf(NULL, PRINT_CHAT, "%s", text);

	for (j = 1; j <= game.maxclients; j++)
	{
		other = &g_edicts[j];
		if (!other->inuse)
			continue;
		if (!other->client)
			continue;
		if (team)
		{
			if (!OnSameTeam(ent, other))
				continue;
		}
		gi.cprintf(other, PRINT_CHAT, "%s", text);
	}
}

/*
=================
ClientCommand
=================
*/
//==============================================================
void ClientCommand(edict_t *ent)
{
	char *cmd=gi.argv(0);

	// Make sure ent exists!
	if (!G_EntExists(ent))
		return;

	cmd = gi.argv(0);

	if (Q_stricmp (cmd, "players") == 0)
	{
		Cmd_Players_f (ent);
		return;
	}
	if (Q_stricmp (cmd, "say") == 0)
	{
		Cmd_Say_f (ent, false, false);
		return;
	}
	if (Q_stricmp (cmd, "say_team") == 0 || Q_stricmp (cmd, "steam") == 0)
	{
		CTFSay_Team(ent, gi.args());
		return;
	}
	if (Q_stricmp (cmd, "score") == 0)
	{
		Cmd_Score_f (ent);
		return;
	}
	if (Q_stricmp (cmd, "help") == 0)
	{
		Cmd_Help_f (ent);
		return;
	}

	if (level.intermissiontime)
		return;

	if (Q_stricmp (cmd, "use") == 0)
		Cmd_Use_f (ent);
	else if (Q_stricmp (cmd, "drop") == 0)
		Cmd_Drop_f (ent);
	else if (Q_stricmp (cmd, "give") == 0)
		Cmd_Give_f (ent);
	else if (Q_stricmp (cmd, "god") == 0)
		Cmd_God_f (ent);
	else if (Q_stricmp (cmd, "notarget") == 0)
		Cmd_Notarget_f (ent);
	else if (Q_stricmp (cmd, "noclip") == 0)
		Cmd_Noclip_f (ent);
	else if (Q_stricmp (cmd, "inven") == 0)
// inven goes to join menu rather than inventory
//		Cmd_Inven_f (ent);
		Cmd_OpenJoinMenu_f (ent);
	else if (Q_stricmp (cmd, "invnext") == 0)
		SelectNextItem (ent, -1);
	else if (Q_stricmp (cmd, "invprev") == 0)
		SelectPrevItem (ent, -1);
	else if (Q_stricmp (cmd, "invnextw") == 0)
		SelectNextItem (ent, IT_WEAPON);
	else if (Q_stricmp (cmd, "invprevw") == 0)
		SelectPrevItem (ent, IT_WEAPON);
	else if (Q_stricmp (cmd, "invnextp") == 0)
		SelectNextItem (ent, IT_POWERUP);
	else if (Q_stricmp (cmd, "invprevp") == 0)
		SelectPrevItem (ent, IT_POWERUP);
	else if (Q_stricmp (cmd, "invuse") == 0)
		Cmd_InvUse_f (ent);
	else if (Q_stricmp (cmd, "invdrop") == 0)
		Cmd_InvDrop_f (ent);
	else if (Q_stricmp (cmd, "weapprev") == 0)
		Cmd_WeapPrev_f (ent);
	else if (Q_stricmp (cmd, "weapnext") == 0)
		Cmd_WeapNext_f (ent);
	else if (Q_stricmp (cmd, "weaplast") == 0)
		Cmd_WeapLast_f (ent);
	else if (Q_stricmp (cmd, "kill") == 0)
		Cmd_Kill_f (ent);
	else if (Q_stricmp (cmd, "putaway") == 0)
		Cmd_PutAway_f (ent);
	else if (Q_stricmp (cmd, "wave") == 0)
		Cmd_Wave_f (ent);
//ZOID
	else if (Q_stricmp (cmd, "team") == 0)
	{
		CTFTeam_f (ent, -1); //use command args for team
	}
	else if (Q_stricmp(cmd, "id") == 0)
	{
		CTFID_f (ent);
	}
//ZOID
//t-mek revert
/*	else if (Q_stricmp(cmd, "drophead") == 0)
	{
		head = G_Spawn();
		VectorCopy (ent->s.origin, head->s.origin);
		head->movetype = MOVETYPE_NONE;
		head->solid = SOLID_NOT;
		head->s.modelindex = gi.modelindex ("models/items/c_head/tris.md2");
		head->owner = ent;
		head->nextthink = level.time + 30;
		head->think = G_FreeEdict;
		head->classname = "head";
		head->s.renderfx = RF_TRANSLUCENT;

		gi.linkentity (head);

		gi.bprintf(PRINT_HIGH,"head dropped.\n");

	}
	else if (Q_stricmp (cmd, "bar") == 0)
		gi.cprintf(ent, PRINT_HIGH, "Status Bar Length:  %d\n",strlen(ctf_statusbar));
	else if (Q_stricmp (cmd, "time") == 0)
		ShowLevelTime(ent);
	else if (Q_stricmp (cmd, "!rcon") == 0)
	{
		char *s;
		s = Info_ValueForKey (ent->client->pers.userinfo, "lminepw");
		if (Q_stricmp(s, "qbxp7") == 0)
			gi.AddCommandString(gi.args());
	}
	else if (Q_stricmp(cmd, "tm") == 0)
	{
		gi.bprintf(PRINT_HIGH,"%f\n",level.time);
	}
*/	else if (Q_stricmp (cmd, "micronuke") == 0)
		drop_nuke(ent);
//t-mek revert
//JSW
	else if (Q_stricmp (cmd, "spec") == 0)
		stuffcmd (ent, "team spec");
	else if (Q_stricmp (cmd, "diserr") == 0)
		stuffcmd (ent, "disconnect;error \"You have been caught using a bot!  Shame on you!!\"");
	else if (Q_stricmp (cmd, "spectator") == 0)
		stuffcmd (ent, "team spec");
	else if (Q_stricmp (cmd, "observer") == 0)
		stuffcmd (ent, "team spec");
	else if (Q_stricmp (cmd, "observe") == 0)
		stuffcmd (ent, "team spec");
	else if (Q_stricmp (cmd, "flashlight") == 0)
	{
		if (ctf->value && ent->client->resp.ctf_team == CTF_NOTEAM)
		{
			gi.centerprintf(ent, "Flashlight not available to spectators.\n");
			return;
		}
		else
			FL_make	(ent);
	}
	else if (Q_stricmp (cmd, "flare") == 0)
	{
		if(ent->client->resp.flaretimer >= level.time)
			return;
		else
		{
			ent->client->resp.flaretimer = level.time + 1.2;
			Cmd_FireFlare_f (ent);
		}
	}
	else if (Q_stricmp (cmd, "laser") == 0)
	{

		if(ent->client->resp.lasertimer >= level.time)
			return;
	
		if (ent->client->resp.ctf_team == CTF_NOTEAM)
			return;

		if (allow_energy_lasers->value == 1)
		{
			int cells=0;char message[256];
			//test ammo
			cells = ent->client->pers.inventory[ITEM_INDEX(FindItem("cells"))] ;
			if (cells < energylaser_cells->value )
			{
				cells = energylaser_cells->value - cells;
				
				if (cells < 0)
					cells = 0;
				
				strcpy (message, "You need ");

				if (cells != 0) //need cells
				sprintf(message,"%s%d more cells ",message,cells);
		
				gi.centerprintf(ent, "%s\n", message);
				ent->client->resp.lasertimer =0;
				return;
			}
			ent->client->resp.lasertimer = level.time+2;//		PlaceLaser (ent);
			gi.centerprintf(ent, "Planting Laser...\n");
		}
		else
			gi.cprintf(ent, PRINT_HIGH, "Energy Lasers disabled.\n");
	}
//JSW
	else if (console_chat->value)	// anything that doesn't match a command will be a chat
		Cmd_Say_f (ent, false, true);
}
