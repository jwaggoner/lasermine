
#include "g_local.h"

// Flashlight by Dustin Ridenbaugh, TheGunslinger@ecr.net
/*
=================
FL_make
make the dang thing
=================
*/

void FL_make	(edict_t *self)
{
	vec3_t	start,forward,right,end;

 	if ( self->flashlight )
	{
		G_FreeEdict(self->flashlight);
		self->flashlight = NULL;
		gi.sound (self, CHAN_VOICE, gi.soundindex ("weapons/noammo.wav"), 1, ATTN_NORM, 0);
		return;
	}

	AngleVectors (self->client->v_angle, forward, right, NULL);

	VectorSet(end,100 , 0, 0);
	G_ProjectSource (self->s.origin, end, forward, right, start);

	self->flashlight = G_Spawn ();
	self->flashlight->owner = self;
	self->flashlight->movetype = MOVETYPE_NOCLIP;
	self->flashlight->solid = SOLID_NOT;
	self->flashlight->classname = "flashlight";
	self->flashlight->s.modelindex = gi.modelindex ("models/objects/laser/tris.md2");	// HEY KIDDYS NOTE THIS
	self->flashlight->s.skinnum = 0;
	self->flashlight->s.effects |= EF_HYPERBLASTER;		// Other effects can be used here, such as flag1, but these look corney and 
								// dull. Try stuff and tell me if you find anything cool
	self->flashlight->think = FL_think;
	self->flashlight->nextthink = level.time + 0.1;
	gi.sound (self, CHAN_VOICE, gi.soundindex ("weapons/noammo.wav"), 1, ATTN_NORM, 0);
}

/*
=================
FL_make
make the dang thing
=================
*/

void FL_think (edict_t *self)
{
	vec3_t start,end,endp,offset;
	vec3_t forward,right,up;
	trace_t tr;

	AngleVectors (self->owner->client->v_angle, forward, right, up);

	VectorSet(offset,24 , 6, self->owner->viewheight-7);
	G_ProjectSource (self->owner->s.origin, offset, forward, right, start);
	VectorMA(start,8192,forward,end);

	tr = gi.trace (start,NULL,NULL, end,self->owner,CONTENTS_SOLID|CONTENTS_MONSTER|CONTENTS_DEADMONSTER);

	if (tr.fraction != 1)
	{
		VectorMA(tr.endpos,-4,forward,endp);
		VectorCopy(endp,tr.endpos);
	}

	if ((tr.ent->svflags & SVF_MONSTER) || (tr.ent->client))
	{
		if ((tr.ent->takedamage) && (tr.ent != self->owner))
		{
			self->s.skinnum = 1;
		}
	}
	else
		self->s.skinnum = 0;

	vectoangles(tr.plane.normal,self->s.angles);
	VectorCopy(tr.endpos,self->s.origin);

	gi.linkentity (self);
	self->nextthink = level.time + 0.1;
}

//
//
void flare_sparks(edict_t *self) 
{ 
    
	// Spawn some sparks.  This isn't net-friendly at all, but will 
    // be fine for single player. 
    gi.WriteByte (svc_temp_entity); 
	gi.WriteByte (TE_BLASTER); 
	gi.WritePosition (self->s.origin); 

	gi.WriteDir (vec3_origin); 

	gi.multicast (self->s.origin, MULTICAST_PVS); 
} 

static void Flare_Touch (edict_t *ent, edict_t *other, cplane_t *plane, csurface_t *surf)
{
	if (other == ent->owner)
		return;

	if (ent->waterlevel == 3)
	{
		G_FreeEdict (ent);
		return;
	}

	if (other->flags & FL_INWATER)
	{
		G_FreeEdict (ent);
		return;
	}

	if (surf && (surf->flags & SURF_SKY))
	{
		gi.sound (ent, CHAN_VOICE, gi.soundindex ("world/land.wav"), 1, ATTN_NORM, 0);
		flare_sparks (ent);
		G_FreeEdict (ent);
		return;
	}
	
	if (!other->takedamage)
	{
		gi.sound (ent, CHAN_VOICE, gi.soundindex ("weapons/lashit.wav"), 1, ATTN_NORM, 0);
				
		// At this point, the grenade has hit a surface
		
		VectorClear (ent->velocity) ;
		VectorClear (ent->avelocity) ;
		
		// We don't want the grenade to be affected by the big G.
		ent->movetype = MOVETYPE_NONE;
		
		return;
	}
	
	gi.sound (ent, CHAN_VOICE, gi.soundindex ("world/land.wav"), 1, ATTN_NORM, 0);
	flare_sparks (ent);
	G_FreeEdict (ent);
}

void Flare_Think2(edict_t *ent)
{
	gi.sound (ent, CHAN_VOICE, gi.soundindex ("world/land.wav"), 1, ATTN_NORM, 0);
	flare_sparks (ent);
	G_FreeEdict (ent);
}

void Flare_Think(edict_t *ent)
{
	
	//	ent->s.renderfx |= RF_FULLBRIGHT;
	ent->nextthink=level.time+10;
	ent->think=Flare_Think2;
	
}

void Flare_Die(edict_t *self, edict_t *inflictor, edict_t *attacker, int damage, vec3_t point)
{
	self->takedamage=DAMAGE_NO;
	self->nextthink=level.time+.1;
	self->think=Flare_Think2;
}	

void fire_flare (edict_t *self, vec3_t start, vec3_t aimdir, int speed)
{
	edict_t *flare;
	vec3_t  dir;
	vec3_t  forward, right, up;
	
	vectoangles (aimdir, dir);
	AngleVectors (dir, forward, right, up);
	
	flare = G_Spawn();
	VectorCopy (start, flare->s.origin);
	VectorScale (aimdir, speed, flare->velocity);
	VectorMA (flare->velocity, 200 + crandom() * 10.0, up, flare->velocity);
	VectorMA (flare->velocity, crandom() * 10.0, right, flare->velocity);
	VectorSet (flare->avelocity, 300, 300, 300);
	flare->movetype = MOVETYPE_BOUNCE;
	flare->clipmask = MASK_SHOT;
	flare->solid = SOLID_BBOX;
	flare->s.effects |= EF_BLASTER;
	flare->s.renderfx |= RF_GLOW;
	VectorClear (flare->mins);
	VectorClear (flare->maxs);
	flare->s.modelindex = gi.modelindex ("models/objects/laser/tris.md2");
	flare->owner = self;
	flare->touch = Flare_Touch;
	flare->nextthink = level.time + 30;
	flare->think = Flare_Think;
	flare->classname = "flare";
	VectorSet(flare->mins, -3, -3, 0);
	VectorSet(flare->maxs, 3, 3, 6);
	flare->mass = 2;
	flare->health = 10;
	flare->monsterinfo.aiflags = AI_NOSTEP;
	flare->die=Flare_Die;
	flare->takedamage=DAMAGE_YES;	
	
	gi.linkentity (flare);
}

